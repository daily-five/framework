# DailyFive Framework

>The Framework is in an early stage of development. So, don't use it in a production system. Contributors are welcome.

Yes, it is another lighweight php framework! The DailyFive framework is inspired by the silex and laravel framework.
It is build with [Symfony Components](https://symfony.com/) and [Pimple](https://pimple.symfony.com/).

The main goal of the framework is an easy to read, develop and testable application structure.

## Who should not use the Farmework?

If you want to rapidly prototype an application, don't use this framework. For this purpose use an other framework like [laravel](https://laravel.com/).


## Table of contents
   
* [Installation](#installation)
* [Documentation](#documentation)
    * [Usage](#usage)
* [Service Providers](#service-providers)
* [Tests](#tests)
* [Contributing](#contributing)
* [License](#license)

## Installation

Install daily-five framework with [Composer](http://getcomposer.org/)
```bash
composer require daily-five/framework
```
If you have permission problems at installation, try this [solution](https://gitlab.com/daily-five/framework/issues/1)


## Documentation

### Usage

This is a very basic *Hello World!* example.

Create a new file called `index.php`

```php
<?php
require_once __DIR__.'/vendor/autoload.php';

// Create a Controller
class HomeController extends \DailyFive\Controller\BasicController
{
    protected $msg;

    public function __construct($msg)
    {
        $this->msg = $msg;
    }

    public function index()
    {
        return $this->response($this->msg);
    }
}


// New Application instance
$app = new \DailyFive\Application();


// Register Service Providers
$app->register(new \DailyFive\Provider\LoggerServiceProvider());


$app['msg'] = 'Hello World';


// Register controller
$app[HomeController::class] = function ($app) {
    return new HomeController($app['msg']);
};


// Register Middleware
$app->addMiddleware('logger', function ($app) {
    return new \DailyFive\Middleware\LoggerMiddleware(
        $app['logger']
    );
});


// Register Routes
$app->get('/', 'HomeController::index', array('middleware' => array('logger')));


// Run Application
$app->run();
```

## Service Providers

Available Service Providers

* [plates-service-provider](https://gitlab.com/daily-five/plates-service-provider)

## Tests

To run the test suite, you need [Composer](http://getcomposer.org/) and [PHPUnit](https://phpunit.de/):

```bash
composer install
vendor/bin/phpunit
```

## Contribution

Any type of feedback, pull request or issue is welcome.

We use the [PHP_CodeSniffer](https://github.com/squizlabs/PHP_CodeSniffer) to check for Coding Standard Issues.

### Check Coding Standard

```bash
vendor/bin/phpcs --standard=PSR1,PSR2,PSR12 --colors src
```

### Fix Errors Automatically

```bash
vendor/bin/phpcbf --standard=PSR1,PSR2,PSR12 src
```

## License

The DailyFive Framework is licensed under the MIT license.