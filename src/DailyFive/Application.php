<?php

namespace DailyFive;

use DailyFive\Container\Container;
use DailyFive\Request\Request;
use Symfony\Component\HttpFoundation\Request as SymfonyRequest;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\HttpKernel\TerminableInterface;

/**
 * Class Application
 * @package DailyFive
 */
class Application extends Container implements HttpKernelInterface, TerminableInterface
{
    /**
     * Application constructor.
     *
     * @param array $values
     *
     * @throws \LogicException
     */
    public function __construct(array $values = array())
    {
        parent::__construct();

        // Default values
        $this['namespace.framework.root'] = 'DailyFive';
        $this['namespace.framework.controller'] = 'Controller';
        $this['namespace.app.root'] = 'App';
        $this['namespace.app.controller'] = $this['namespace.framework.controller'];

        $this['request.http_port'] = 80;
        $this['request.https_port'] = 443;
        $this['debug'] = false;
        $this['charset'] = 'UTF-8';
        $this['logger'] = null;
        $this['kernel.logger'] = function () {
            return new \Psr\Log\NullLogger();
        };
        $this['path'] = root_path();

        foreach ($values as $key => $value) {
            $this->offsetSet($key, $value);
        }

        // Default paths
        $this['path.public'] = $this['path'] . '/web';
        $this['path.log'] = $this['path'] . '/tmp/logs';
        $this['path.cache'] = $this['path'] . '/tmp/cache';
        $this['path.storage'] = $this['path'] . '/storage';

        $this->registerProviders();
    }

    /**
     * Register default ServiceProviders
     *
     * @return void
     */
    protected function registerProviders()
    {
        $this->register(new \DailyFive\Provider\HttpKernelServiceProvider());
        $this->register(new \DailyFive\Provider\RoutingServiceProvider());
    }

    /**
     * Run the Application
     *
     * @param \DailyFive\Request\Request|null $request
     *
     * @return void
     */
    public function run(Request $request = null)
    {
        if (null === $request) {
            $request = Request::createFromBase(SymfonyRequest::createFromGlobals());
        }

        $response = $this->handle($request);
        $response->send();
        $this->terminate($request, $response);
    }

    /**
     * Returns whether debugging is enabled and the logger has a \Psr\Log\LoggerInterface
     *
     * @return bool
     */
    public function isDebuggingEnabled()
    {
        return !!$this['debug'];
    }

    /*
    |---------------------------------------------------------------------------
    | Application - kernel part
    |---------------------------------------------------------------------------
    */

    /**
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @param                                           $type
     * @param bool                                      $catch
     *
     * @return mixed
     */
    public function handle(SymfonyRequest $request, $type = HttpKernelInterface::MASTER_REQUEST, $catch = true)
    {
        return $this['http_kernel']->handle($request, $type, $catch);
    }

    /**
     * @param \Symfony\Component\HttpFoundation\Request  $request
     * @param \Symfony\Component\HttpFoundation\Response $response
     *
     * @return void
     */
    public function terminate(SymfonyRequest $request, Response $response)
    {
        // Not needed yet
    }

    /*
    |---------------------------------------------------------------------------
    | Application - middleware part
    |---------------------------------------------------------------------------
    */

    /**
     * Add the middleware to the Application and set the namespace
     *
     * @param string   $name
     * @param \Closure $middleware
     *
     * @return void
     */
    public function addMiddleware($name, \Closure $middleware)
    {
        $namespace = \DailyFive\Middleware\Dispatcher::MIDDLEWARE_NAMESPACE . $name;

        // Register middleware in Application
        $this->offsetSet($namespace, $middleware);
    }

    /*
    |---------------------------------------------------------------------------
    | Application - routing part
    |---------------------------------------------------------------------------
    */

    /**
     * Adds a route for matching GET requests
     *
     * @param string $path
     * @param string $controller
     * @param array  $options
     *
     * @return void
     */
    public function get($path, $controller, array $options = array())
    {
        $this['router']->get($path, $controller, $options);
    }

    /**
     * Adds a route for matching POST requests
     *
     * @param string $path
     * @param string $controller
     * @param array  $options
     *
     * @return void
     */
    public function post($path, $controller, array $options = array())
    {
        $this['router']->post($path, $controller, $options);
    }

    /**
     * Adds a route for matching PUT requests
     *
     * @param string $path
     * @param string $controller
     * @param array  $options
     *
     * @return void
     */
    public function put($path, $controller, array $options = array())
    {
        $this['router']->put($path, $controller, $options);
    }

    /**
     * Adds a route for matching PATCH requests
     *
     * @param string $path
     * @param string $controller
     * @param array  $options
     *
     * @return void
     */
    public function patch($path, $controller, array $options = array())
    {
        $this['router']->patch($path, $controller, $options);
    }

    /**
     * Adds a route for matching DELETE requests
     *
     * @param string $path
     * @param string $controller
     * @param array  $options
     *
     * @return void
     */
    public function delete($path, $controller, array $options = array())
    {
        $this['router']->delete($path, $controller, $options);
    }

    /**
     * Adds a route for each request method
     *
     * @param array  $requestMethods
     * @param string $path
     * @param string $controller
     * @param array  $options
     *
     * @return void
     */
    public function match(array $requestMethods, $path, $controller, array $options = array())
    {
        $this['router']->match($requestMethods, $path, $controller, $options);
    }

    /**
     * Adds a RouteGroup to RouteCollection
     *
     * @param string   $prefix
     * @param array    $options
     * @param \Closure $callback
     *
     * @return void
     */
    public function group($prefix, array $options, \Closure $callback)
    {
        $this['router']->group($prefix, $options, $callback);
    }
}
