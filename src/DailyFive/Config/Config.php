<?php

namespace DailyFive\Config;

use LogicException;

/**
 * Class Config
 * @package DailyFive\Config
 */
class Config
{
    /**
     * @var null|string
     */
    protected $configPath;

    /**
     * @var array
     */
    protected $items = array();

    /**
     * Config constructor.
     *
     * @param null|string $path Path to config files
     */
    public function __construct($path = null)
    {
        if (is_string($path)) {
            $this->loadFiles($path);
        }
    }

    /**
     * @param string $path
     *
     * @return void
     */
    protected function loadFiles($path)
    {
        // Show if path exists
        if (! file_exists($path)) {
            throw new LogicException("Path '{$path}' doesn't exist.");
        }

        // load all config files
        $files = array_diff(scandir($path), array('.', '..', '.gitignore'));
        foreach ($files as $file) {
            $value = include_once $path . DIRECTORY_SEPARATOR . $file;
            $this->set(str_replace(strrchr($file, "."), "", $file), $value);
        }
    }

    /**
     * @param string     $key
     * @param null|mixed $default
     *
     * @return mixed|null
     */
    public function get($key, $default = null)
    {
        $this->isKeyString($key);
        $keys = explode(".", $key);
        return $this->getAssocValueByDot($this->items, $keys, $default);
    }

    /**
     * @param array $arr
     * @param array $keys
     * @param mixed $default
     *
     * @return mixed
     */
    protected function getAssocValueByDot(array $arr, array $keys, $default)
    {
        if (count($keys) === 1) {
            return isset($arr[$keys[0]])
                ? $arr[$keys[0]]
                : $default;
        }

        return isset($arr[$keys[0]])
            ? $this->getAssocValueByDot($arr[$keys[0]], array_slice($keys, 1), $default)
            : $default;
    }

    /**
     * @param string       $key
     * @param string|array $value
     *
     * @return bool
     */
    public function set($key, $value)
    {
        $this->isKeyString($key);

        $keys = explode(".", $key);

        if (count($keys) === 1) {
            $this->items[$keys[0]] = $value;
            return true;
        }
        $normalizedArr = $this->normalizeDotKeyValue(array_slice($keys, 1), $value);

        if (! isset($this->items[$keys[0]]) || ! is_array($this->items[$keys[0]])) {
            $this->items[$keys[0]] = $normalizedArr;
            return true;
        }

        $this->items[$keys[0]] = array_merge($this->items[$keys[0]], $normalizedArr);
        return true;
    }

    /**
     * @param array $keys
     * @param mixed $value
     *
     * @return array
     */
    protected function normalizeDotKeyValue(array $keys, $value)
    {
        $elements = array();
        if (count($keys) === 1) {
            $elements[$keys[0]] = $value;
        } else {
            $elements[$keys[0]] = $this->normalizeDotKeyValue(array_slice($keys, 1), $value);
        }
        return $elements;
    }

    /**
     * @param mixed $key
     *
     * @throws \LogicException
     */
    protected function isKeyString($key)
    {
        if (! is_string($key) || empty($key)) {
            throw new LogicException("The Config-Key must be a string and not empty. Given type: " . gettype($key));
        }
    }
}
