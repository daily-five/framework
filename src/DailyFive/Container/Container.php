<?php

namespace DailyFive\Container;

use ArrayAccess;
use Illuminate\Support\Arr;
use Pimple\Container as PimpleContainer;
use Pimple\Exception\FrozenServiceException;
use Pimple\Exception\UnknownIdentifierException;
use Pimple\ServiceProviderInterface;
use ReflectionClass;

/**
 * Class Container
 * @package DailyFive\Container
 */
class Container extends PimpleContainer implements ArrayAccess
{
    /** @var array */
    protected $aliases = array();

    /**
     * Container constructor.
     *
     * @param array $values
     */
    public function __construct(array $values = array())
    {
        parent::__construct($values);
    }

    /**
     * Sets a parameter or an object.
     *
     * Objects must be defined as Closures.
     *
     * Allowing any PHP callable leads to difficult to debug problems
     * as function names (strings) are callable (creating a function with
     * the same name as an existing parameter would break your container).
     *
     * @param string $id    The unique identifier for the parameter or object
     * @param mixed  $value The value of the parameter or a closure to define an object
     *
     * @throws FrozenServiceException Prevent override of a frozen service
     */
    public function offsetSet($id, $value)
    {
        parent::offsetSet($id, $value);
        $this->setAlias($id);
    }

    /**
     * Gets a parameter or an object.
     *
     * @param string $id The unique identifier for the parameter or object
     *
     * @return mixed The value of the parameter or an object
     *
     * @throws UnknownIdentifierException If the identifier is not defined
     * @throws \ReflectionException
     */
    public function offsetGet($id)
    {
        if (!$this->offsetExists($id) && class_exists($id)) {
            // try to auto resolve class
            $this->resolveClass($id);
        }

        if ($this->hasAlias($id)) {
            $id = $this->getAlias($id);
        }

        return parent::offsetGet($id);
    }

    /**
     * Checks if a parameter or an object is set.
     *
     * @param string $id The unique identifier for the parameter or object
     *
     * @return bool
     */
    public function offsetExists($id)
    {
        $id = $this->hasAlias($id)
            ? $this->getAlias($id)
            : $id;
        return parent::offsetExists($id);
    }

    /**
     * @param string $id
     */
    public function offsetUnset($id)
    {
        if ($this->hasAlias($id)) {
            $this->offsetUnset($this->getAlias($id));
            return;
        } elseif (in_array($id, array_values($this->aliases))) {
            $this->unsetAlias($this->createAliasFromClass($id));
        }
        parent::offsetUnset($id);
    }

    /**
     * Registers a service provider.
     *
     * @param ServiceProviderInterface $provider A ServiceProviderInterface instance
     * @param array                    $values   An array of values that customizes the provider
     *
     * @return static
     */
    public function register(ServiceProviderInterface $provider, array $values = array())
    {
        $provider->register($this);

        // NOTE(ssandriesser): workaround to provide legacy functionality
        if (is_callable([$provider, 'alias'])) {
            $this->aliases = array_merge($this->aliases, $provider->alias());
        }

        foreach ($values as $key => $value) {
            $this[$key] = $value;
        }

        return $this;
    }

    /**
     * Returns all defined aliases
     *
     * @return array
     */
    public function aliases()
    {
        return array_keys($this->aliases);
    }

    /**
     * @param string $id
     *
     * @return mixed
     */
    public function createAliasFromClass($id)
    {
        return Arr::last(explode('\\', trim($id, '\\')));
    }

    /**
     * @param string $alias
     *
     * @return bool
     */
    public function hasAlias($alias)
    {
        return isset($this->aliases[$alias]);
    }

    /**
     * @param string $alias
     *
     * @return mixed
     */
    public function getAlias($alias)
    {
        return $this->hasAlias($alias) ? $this->aliases[$alias] : null;
    }

    /**
     * @param string      $id
     * @param null|string $alias
     */
    public function setAlias($id, $alias = null)
    {
        if ($alias) {
            $this->aliases[$alias] = $id;
        } elseif (class_exists($id)) {
            $this->aliases[$this->createAliasFromClass($id)] = $id;
        }
    }

    /**
     * @param string $alias
     */
    public function unsetAlias($alias)
    {
        unset($this->aliases[$alias]);
    }

    /**
     * @param string $id
     * @param bool   $silent
     *
     * @return bool|void
     * @throws \ReflectionException
     */
    public function resolveClass($id, $silent = false)
    {
        try {
            $reflectedClass = new ReflectionClass($id);
        } catch (\ReflectionException $ex) {
            if ($silent) {
                return false;
            }
            throw $ex;
        }

        $constructor = $reflectedClass->getConstructor();

        $params = [];
        if ($constructor) {
            foreach ($constructor->getParameters() as $parameter) {
                if ($type = $parameter->getType()) {
                    if (class_exists($type->getName())) {
                        $params[] = $this[$type->getName()];
                    } elseif ($type->getName() === 'array') {
                        $params[] = [];
                    } else {
                        $params[] = null;
                    }
                } elseif ($parameter->isOptional()) {
                    $params[] = $parameter->getDefaultValue();
                } else {
                    $params[] = null;
                }
            }
        }

        $this->offsetSet($id, function () use ($reflectedClass, $params) {
            return $reflectedClass->newInstanceArgs($params);
        });
        return true;
    }
}
