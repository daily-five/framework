<?php

namespace DailyFive\Controller;

use \DailyFive\Routing\Controller;
use \Symfony\Component\HttpFoundation\RedirectResponse;
use \Symfony\Component\HttpFoundation\Response;
use \Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class BasicController
 * @package DailyFive\Controller
 */
class BasicController extends Controller
{
    /**
     * Returns the given data as a response
     *
     * @param string $content
     * @param int    $status
     * @param array  $headers
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    protected function response($content = '', $status = 200, $headers = array())
    {
        return new Response($content, $status, $headers);
    }

    /**
     * Returns the given data as a json response
     *
     * @param array|string $data
     * @param int          $status
     * @param array        $headers
     * @param bool         $json
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    protected function json($data = null, $status = 200, $headers = array(), $json = false)
    {
        if (is_array($data) || $json) {
        } else {
            $data = (array)$data;
        }
        return new JsonResponse($data, $status, $headers, $json);
    }

    /**
     * Returns the a content as downloadable file
     *
     * @link https://tools.ietf.org/html/rfc6838
     *
     * @param string $contentType
     * @param string $filename
     * @param string $content
     * @param int    $status
     * @param array  $headers
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    protected function download($contentType, $filename, $content = '', $status = 200, $headers = array())
    {
        $response = $this->response($content, $status, $headers);
        $response->headers->set('Content-Type', $contentType);
        $response->headers->set('Content-Disposition', sprintf('attachment; filename="%s"', $filename));
        return $response;
    }

    /**
     * Return a redirect response
     *
     * @param string $url
     * @param int    $status
     * @param array  $headers
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    protected function redirect($url, $status = 302, array $headers = array())
    {
        return new RedirectResponse($url, $status, $headers);
    }
}
