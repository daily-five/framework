<?php
/*
|--------------------------------------------------------------------------
| Array helper function
|--------------------------------------------------------------------------
*/

use Illuminate\Support\Arr;

if (! function_exists('arr_first')) {
    /**
     * Return the first element in an array passing a given truth test.
     *
     * @deprecated Arr::first() should be used directly instead
     *
     * @param array         $array
     * @param callable|null $callback
     * @param mixed         $default
     *
     * @return mixed
     */
    function arr_first($array, callable $callback = null, $default = null)
    {
        return Arr::first($array, $callback, $default);
    }
}
