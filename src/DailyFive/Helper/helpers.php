<?php
/*
|--------------------------------------------------------------------------
| Helper functions
|--------------------------------------------------------------------------
*/

if (! function_exists('root_path')) {
    /**
     * Returns the application root path
     *
     * @return string
     */
    function root_path()
    {
        if (file_exists(__DIR__ . '/../../../../../../vendor')) {
            return dirname(__DIR__, 6) . '/';
        }
        return dirname(__DIR__, 3) . '/';
    }
}

if (! function_exists('public_path')) {
    /**
     * Returns a given file as full path relative from public directory.
     *
     * @param string $file
     *
     * @return string
     */
    function public_path($file = '')
    {
        $publicPath = root_path() . '/web';
        return sprintf('%s/%s', $publicPath, trim($file, '/'));
    }
}

if (! function_exists('database_path')) {
    /**
     * Returns a given file as full path relative from database directory.
     *
     * @param string $file
     *
     * @return string
     */
    function database_path($file = '')
    {
        $publicPath = root_path() . '/database';
        return sprintf('%s/%s', $publicPath, trim($file, '/'));
    }
}

if (! function_exists('storage_path')) {
    /**
     * Returns a given file as full path relative from storage directory.
     *
     * @param string $file
     *
     * @return string
     */
    function storage_path($file = '')
    {
        $publicPath = root_path() . '/storage';
        return sprintf('%s/%s', $publicPath, trim($file, '/'));
    }
}

if (! function_exists('mix')) {
    /**
     * Get the path to a versioned Mix file.
     *
     * @param  string  $path
     *
     * @return string
     * @throws \Exception
     */
    function mix($path)
    {
        static $manifest = array();

        if (! $manifest) {
            if (! file_exists($manifestPath = public_path('/mix-manifest.json'))) {
                throw new Exception('The Mix manifest does not exist.');
            }
            $manifest = json_decode(file_get_contents($manifestPath), true);
        }

        $path = "/" . trim($path, '/');

        if (! array_key_exists($path, $manifest)) {
            var_dump($manifest);
            throw new Exception(
                "Unable to locate Mix file: {$path}. Please check your " .
                'webpack.mix.js output paths and try again.'
            );
        }
        return file_exists(public_path('/hot'))
            ? "http://localhost:8080{$manifest[$path]}"
            : $manifest[$path];
    }
}

if (! function_exists('slugify')) {
    /**
     * Matches only the following char's -> [a-zA-Z0-9]
     *
     * @deprecated str_slugify() should be used directly instead
     *
     * @param  string $str
     *
     * @return string
     */
    function slugify($str = '')
    {
        preg_match_all("/(\\w+)/mi", $str, $matches);
        return implode('_', $matches[1]);
    }
}

if (! function_exists('dotenv')) {
    /**
     * Gets the value of an environment variable.
     *
     * @param  string  $key
     * @param  mixed   $default
     * @return mixed
     */
    function dotenv($key, $default = null)
    {
        $value = getenv($key);

        if ($value === false) {
            return value($default);
        }

        switch (strtolower($value)) {
            case 'true':
            case '(true)':
                return true;
            case 'false':
            case '(false)':
                return false;
            case 'empty':
            case '(empty)':
                return '';
            case 'null':
            case '(null)':
                return;
        }

        if (($valueLength = strlen($value)) > 1 && strpos($value, '"') === 0 && $value[$valueLength - 1] === '"') {
            return substr($value, 1, -1);
        }

        return $value;
    }
}
