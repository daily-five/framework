<?php
/*
|--------------------------------------------------------------------------
| String helper function
|--------------------------------------------------------------------------
*/

use Illuminate\Support\Str;

if (! function_exists('str_contains')) {
    /**
     * Determine if a given string contains a given substring
     *
     * @deprecated Str::contains() should be used directly instead
     *
     * @param  string       $haystack
     * @param  string|array $needles
     *
     * @return bool
     */
    function str_contains($haystack, $needles)
    {
        return Str::contains($haystack, $needles);
    }
}

if (! function_exists('str_slugify')) {
    /**
     * Matches only the following char's -> [a-zA-Z0-9]
     *
     *
     * @param  string $str
     *
     * @return string
     */
    function str_slugify($str = '')
    {
        return slugify($str);
    }
}

if (! function_exists('str_is')) {
    /**
     * Determine if a given string matches a given pattern.
     *
     * @deprecated Str::is() should be used directly instead
     *
     * @param string $pattern
     * @param string $value
     *
     * @return bool
     */
    function str_is($pattern, $value)
    {
        return Str::is($pattern, $value);
    }
}

if (! function_exists('str_studly')) {
    /**
     * @param string $str
     *
     * @deprecated Str::studly() should be used directly instead
     *
     * @return string
     */
    function str_studly($str = '')
    {
        return Str::studly($str);
    }
}

if (! function_exists('str_camel')) {
    /**
     * @param string $str
     *
     * @deprecated Str::camel() should be used directly instead
     *
     * @return string
     */
    function str_camel($str = '')
    {
        return Str::camel($str);
    }
}
