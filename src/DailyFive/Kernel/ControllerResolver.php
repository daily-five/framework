<?php

namespace DailyFive\Kernel;

use BadMethodCallException;
use DailyFive\Application;
use DailyFive\Routing\Controller;
use LogicException;
use ReflectionException;
use ReflectionMethod;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class ControllerResolver
 * @package DailyFive\Kernel
 */
class ControllerResolver
{
    /**
     * @var \DailyFive\Application
     */
    protected $app;

    /**
     * The Controller name
     * @var string
     */
    protected $controller;

    /**
     * The Method name
     * @var string
     */
    protected $method;

    /**
     * The Method arguments
     * @var array
     */
    protected $arguments = array();

    /**
     * The Route middleware
     * @var array
     */
    protected $middleware = array();

    /**
     * ControllerResolver constructor.
     *
     * @param \DailyFive\Application $app
     */
    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    /**
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @param array                                     $match
     *
     * @return void
     * @throws \ReflectionException
     */
    public function resolve(Request $request, array $match)
    {
        /**
         * Extract variables
         * @var string $_controller
         * @var string $_method
         * @var array $options
         */
        extract($match, EXTR_SKIP);

        // FIXME(ssandriesser): find a cleaner solution
        // Check if controller exists
        if (isset($this->app[$this->getControllerClass($_controller)])) {
            $controller_name = $this->getControllerClass($_controller);
            $containerIdentifier = $controller_name;
        } elseif (isset($this->app[$_controller])) {
            $controller_name = get_class($this->app[$_controller]);
            $containerIdentifier = $_controller;
        } else {
            if ($this->app->resolveClass($this->getControllerClass($_controller), true)) {
                $controller_name = $this->getControllerClass($_controller);
                $containerIdentifier = $controller_name;
            } else {
                throw new LogicException("{$_controller} not found!");
            }
        }


        // Check if Controller is an Instance of Controller
        if (! $this->app[$containerIdentifier] instanceof Controller) {
            throw new LogicException("{$controller_name} must be an instance of \DailyFive\Routing\Controller");
        }

        // Create a reflection from the target controller and the target method
        try {
            $reflection = new ReflectionMethod($controller_name, $_method);
        } catch (ReflectionException $ex) {
            throw new BadMethodCallException($ex->getMessage());
        }

        // Resolve the arguments for controller method
        $this->controller = $containerIdentifier;
        $this->method = $_method;

        // Check if target method is public.
        // Otherwise throw an exception
        if (! $reflection->isPublic()) {
            throw new LogicException("Method '{$this->method}' from '{$this->controller}' is not public");
        }

        $middleware = 'middleware';
        $this->middleware = isset($options[$middleware]) ? (array)$options[$middleware] : array();

        // Fill the method arguments list
        foreach ($reflection->getParameters() as $parameter) {
            $parameter_name = $parameter->getName();

            if ('request' === $parameter_name) {
                $this->arguments[] = $request;
                continue;
            }

            $this->arguments[] = isset(${$parameter_name}) ? ${$parameter_name} : null;
        }
    }

    /**
     * Returns the resolved data
     *
     * The resolved data contains an assoc array with:
     * - controller: string
     * - method: string
     * - arguments: array
     * - middleware: array
     *
     * @return array
     */
    public function getResolvedData()
    {
        return array(
            'controller' => $this->getControllerName(),
            'method' => $this->getMethodName(),
            'arguments' => $this->getArguments(),
            'middleware' => $this->getMiddleware(),
        );
    }

    /**
     * @return string
     */
    public function getControllerName()
    {
        return $this->controller;
    }

    /**
     * @return string
     */
    public function getMethodName()
    {
        return $this->method;
    }

    /**
     * @return array
     */
    public function getArguments()
    {
        return $this->arguments;
    }

    /**
     * @return array
     */
    public function getMiddleware()
    {
        return $this->middleware;
    }

    /**
     * @param string $name
     * @param string $forNamespace
     *
     * @return string
     */
    protected function getControllerClass($name, $forNamespace = 'app')
    {
        return $this->app["namespace.{$forNamespace}.root"]
            . '\\' . $this->app["namespace.{$forNamespace}.controller"]
            . '\\' . $name;
    }
}
