<?php

namespace DailyFive\Kernel;

use DailyFive\Response\ExceptionResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;

/**
 * Class HttpKernel
 * @package DailyFive\Kernel
 */
class HttpKernel implements HttpKernelInterface
{
    const ATTR_FORM_METHOD = '__method';
    
    /**
     * @var \DailyFive\Application
     */
    private $app;

    /**
     * @var \Symfony\Component\Routing\RequestContext
     */
    private $requestContext;

    /**
     * @var \DailyFive\Routing\UrlMatcher
     */
    private $urlMatcher;

    /**
     * @var \Symfony\Component\Routing\RouteCollection
     */
    private $routes;

    /**
     * @var \DailyFive\Kernel\ControllerResolver
     */
    private $controllerResolver;

    /**
     * @var \DailyFive\Middleware\Dispatcher
     */
    private $middlewareDispatcher;

    /**
     * @var array
     */
    private $urlMatch;

    /**
     * HttpKernel constructor.
     *
     * @param \DailyFive\Application                     $app
     * @param \Symfony\Component\Routing\RequestContext  $requestContext
     * @param \DailyFive\Routing\UrlMatcher              $urlMatcher
     * @param \Symfony\Component\Routing\RouteCollection $routes
     * @param \DailyFive\Kernel\ControllerResolver       $controllerResolver
     * @param \DailyFive\Middleware\Dispatcher           $middlewareDispatcher
     */
    public function __construct(
        \DailyFive\Application $app,
        \Symfony\Component\Routing\RequestContext $requestContext,
        \DailyFive\Routing\UrlMatcher $urlMatcher,
        \Symfony\Component\Routing\RouteCollection $routes,
        \DailyFive\Kernel\ControllerResolver $controllerResolver,
        \DailyFive\Middleware\Dispatcher $middlewareDispatcher
    ) {
        $this->app = $app;
        $this->requestContext = $requestContext;
        $this->urlMatcher = $urlMatcher;
        $this->routes = $routes;
        $this->controllerResolver = $controllerResolver;
        $this->middlewareDispatcher = $middlewareDispatcher;
    }

    /**
     * Handles a Request to convert it to a Response.
     *
     * When $catch is true, the implementation must catch all exceptions
     * and do its best to convert them to a Response instance.
     *
     * @param Request $request A Request instance
     * @param int     $type    The type of the request
     *                         (one of HttpKernelInterface::MASTER_REQUEST or HttpKernelInterface::SUB_REQUEST)
     * @param bool    $catch   Whether to catch exceptions or not
     *
     * @return Response A Response instance
     *
     * @throws \LogicException When an Exception occurs during processing
     */
    public function handle(Request $request, $type = self::MASTER_REQUEST, $catch = true)
    {
        $request->headers->set('X-Php-Ob-Level', ob_get_level());

        try {
            $response = $this->rawHandle($request);

            if (! ($response instanceof Response)) {
                throw new \LogicException(var_export($response, true) . " is no instance of Response");
            }

            return $response;
        } catch (ResourceNotFoundException $ex) {
            // TODO: Redirect to 404 page if application is not in debug mode
            // TODO: Create a default 404 page
            // TODO: Return a 404 JsonResponse for api calls

            return new ExceptionResponse($ex, 404);
        } catch (\Exception $ex) {
            return new ExceptionResponse($ex, 500);
        }
    }

    /**
     * @param \Symfony\Component\HttpFoundation\Request $request
     *
     * @return mixed
     */
    public function rawHandle(Request $request)
    {
        if ($request->get(self::ATTR_FORM_METHOD)) {
            $request->setMethod($request->get(self::ATTR_FORM_METHOD));
        }
        
        // Creates a request context by using the request object
        $context = $this->requestContext->fromRequest($request);

        // Use the context and match it with the routes collection
        $this->urlMatcher->setContext($context);
        $this->urlMatch = $this->urlMatcher->matchRoutesCollection($request->getPathInfo(), $this->routes);

        if (! is_array($this->urlMatch)) {
            throw new ResourceNotFoundException("No Route matches the path '{$request->getPathInfo()}'");
        }

        // Resolve Controller arguments
        $this->controllerResolver->resolve($request, $this->urlMatch);

        // Set middleware stack for middleware dispatcher
        $this->middlewareDispatcher->setStack($this->controllerResolver->getMiddleware());
        return $this->middlewareDispatcher->dispatch($this, $request);
    }

    /**
     * Calls the target Controller on the basis of the request
     *
     * @param \Symfony\Component\HttpFoundation\Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function callController(Request $request)
    {
        $resolved = $this->controllerResolver->getResolvedData();

        // Call target method with needed arguments
        return $this->app[$resolved['controller']]->callAction($resolved['method'], $resolved['arguments']);
    }
}
