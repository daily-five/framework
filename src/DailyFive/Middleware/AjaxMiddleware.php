<?php

namespace DailyFive\Middleware;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class AjaxMiddleware
 * @package DailyFive\Middleware
 */
class AjaxMiddleware implements MiddlewareInterface
{
    /**
     * AjaxMiddleware constructor.
     */
    public function __construct()
    {
    }

    /**
     * @param \Symfony\Component\HttpFoundation\Request  $request  the request
     * @param \Closure                                   $next     the next middleware
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function handle(Request $request, $next)
    {
        if ($request->isXmlHttpRequest()) {
            return $next($request);
        }

        $msg = "The Request must be an AJAX request. There is no 'X-Requested-With' header found."; // [i18n]
        return new JsonResponse(array(
            'message' => $msg,
            'description' => $msg,
        ), 422);
    }
}
