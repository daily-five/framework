<?php

namespace DailyFive\Middleware;

use DailyFive\Application;
use DailyFive\Kernel\HttpKernel;
use LogicException;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class Dispatcher
 * @package DailyFive\Middleware\Dispatcher
 */
class Dispatcher
{
    const MIDDLEWARE_NAMESPACE = 'middleware.';

    /**
     * @var \DailyFive\Application
     */
    protected $app;

    /**
     * @var \DailyFive\Kernel\HttpKernel
     */
    protected $kernel;

    /**
     * @var mixed[] unresolved middleware stack
     */
    protected $stack;

    /**
     * Dispatcher constructor.
     *
     * @param \DailyFive\Application $app
     * @param array                  $stack
     */
    public function __construct(Application $app, array $stack)
    {
        $this->app = $app;
        $this->stack = $stack;
    }

    /**
     * @param \DailyFive\Kernel\HttpKernel                    $kernel
     * @param \Symfony\Component\HttpFoundation\Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function dispatch(HttpKernel $kernel, Request $request)
    {
        $this->kernel = $kernel;
        $resolved = $this->resolve(0);

        return $resolved($request);
    }

    /**
     * @param int $index
     *
     * @return \Closure
     * @throws \LogicException
     */
    protected function resolve($index)
    {
        // Set current middleware namespace
        $namespace = isset($this->stack[$index]) ? self::MIDDLEWARE_NAMESPACE . $this->stack[$index] : null;

        $app = $this->app;
        $kernel = $this->kernel;

        // At the end of the middleware stack, call the controller
        if ($namespace === null) {
            return function ($request) use ($kernel) {
                return $kernel->callController($request);
            };
        } elseif (isset($this->app[$namespace])) { // Check if middleware exists as well in the container
            $dispatcher = $this;

            return function ($request) use ($namespace, $index, $app, $dispatcher) {

                return $app[$namespace]->handle($request, $dispatcher->resolve($index + 1));
            };
        } else {
            throw new LogicException("Middleware '{$this->stack[$index]}' not found");
        }
    }

    /**
     * @param array $stack
     *
     * @return void
     */
    public function setStack(array $stack)
    {
        $this->stack = $stack;
    }

    /**
     * Add the middleware namespace
     *
     * @param $name
     *
     * @return void
     */
    public function add($name)
    {
        $this->stack[] = $name;
    }
}
