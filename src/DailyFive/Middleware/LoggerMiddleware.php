<?php

namespace DailyFive\Middleware;

use Symfony\Component\HttpFoundation\Request;

/**
 * Class LoggerMiddleware
 * @package DailyFive\Middleware
 */
class LoggerMiddleware implements MiddlewareInterface
{
    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    /**
     * LoggerMiddleware constructor.
     *
     * @param \Psr\Log\LoggerInterface $logger
     */
    public function __construct(
        \Psr\Log\LoggerInterface $logger
    ) {
        $this->logger = $logger;
    }

    /**
     * @todo exclude passwords from logging
     *
     * @param \Symfony\Component\HttpFoundation\Request  $request  the request
     * @param \Closure                                   $next     the next middleware
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function handle(Request $request, $next)
    {
        $incomingRequest = array(
            'request' => $request->request->all(),
            'query' => $request->query->all(),
            'cookies' => $request->cookies->all(),
            'attributes' => $request->attributes->all(),
            'files' => $request->files->all(),
            'server' => $request->server->all(),
            'headers' => $request->headers->all(),
        );
        $this->logger->debug('incoming request', $incomingRequest);

        return $next($request);
    }
}
