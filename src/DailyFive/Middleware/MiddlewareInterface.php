<?php

namespace DailyFive\Middleware;

use Symfony\Component\HttpFoundation\Request;

/**
 * Class MiddlewareInterface
 * @package DailyFive\Middleware
 */
interface MiddlewareInterface
{
    /**
     * @param \Symfony\Component\HttpFoundation\Request  $request  the request
     * @param \Closure                                   $next     the next middleware
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function handle(Request $request, $next);
}
