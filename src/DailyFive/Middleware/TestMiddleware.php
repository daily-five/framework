<?php

namespace DailyFive\Middleware;

use Symfony\Component\HttpFoundation\Request;

/**
 * Class TestMiddleware
 * @package DailyFive\Middleware
 */
class TestMiddleware implements MiddlewareInterface
{
    /**
     * TestMiddleware constructor.
     */
    public function __construct()
    {
    }

    /**
     * @param \Symfony\Component\HttpFoundation\Request  $request  the request
     * @param \Closure                                   $next     the next middleware
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function handle(Request $request, $next)
    {
        return $next($request);
    }
}
