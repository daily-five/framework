<?php

namespace DailyFive\Provider;

use DailyFive\Config\Config;
use Pimple\Container;

/**
 * Class ConfigServiceProvider
 * @package DailyFive\Provider
 */
class ConfigServiceProvider implements \Pimple\ServiceProviderInterface
{
    /**
     * Registers services on the given container.
     *
     * This method should only be used to configure services and parameters.
     * It should not get services.
     *
     * @param Container $pimple A container instance
     */
    public function register(Container $pimple)
    {
        $pimple['path.config'] = $pimple['path'] . '/config';

        $pimple[Config::class] = function ($pimple) {
            return new Config(
                $pimple['path.config']
            );
        };
    }

    /**
     * @return array
     */
    public function alias()
    {
        return [
            'config' => Config::class,
        ];
    }
}
