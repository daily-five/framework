<?php

namespace DailyFive\Provider;

use Pimple\Container;
use Pimple\ServiceProviderInterface;
use Symfony\Component\Dotenv\Dotenv;

/**
 * Class DotenvServiceProvider
 * @package DailyFive\Provider
 */
class DotenvServiceProvider implements ServiceProviderInterface
{

    /**
     * Registers services on the given container.
     *
     * This method should only be used to configure services and parameters.
     * It should not get services.
     *
     * @param Container $pimple A container instance
     */
    public function register(Container $pimple)
    {
        $pimple[Dotenv::class] = function () {
            return new Dotenv();
        };
        $pimple[Dotenv::class]->loadEnv(root_path() . '/.env');
    }
}
