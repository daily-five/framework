<?php

namespace DailyFive\Provider;

use League\Flysystem\Adapter\Local;
use League\Flysystem\Filesystem;
use Pimple\Container;

/**
 * Class FilesystemServiceProvider
 * @package DailyFive\Provider
 */
class FilesystemServiceProvider implements \Pimple\ServiceProviderInterface
{
    /**
     * Registers services on the given container.
     *
     * This method should only be used to configure services and parameters.
     * It should not get services.
     *
     * @param Container $pimple A container instance
     */
    public function register(Container $pimple)
    {
        $pimple[Local::class] = function ($pimple) {
            return new Local($pimple['path.storage']);
        };

        $pimple[Filesystem::class] = function ($pimple) {
            return new Filesystem($pimple[Local::class]);
        };
    }

    /**
     * @return array
     */
    public function alias()
    {
        return [
            'filesystem.adapter' => Local::class,
            'filesystem' => Filesystem::class,
        ];
    }
}
