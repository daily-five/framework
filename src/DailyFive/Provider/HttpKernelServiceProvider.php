<?php

namespace DailyFive\Provider;

use Pimple\Container;

/**
 * Class HttpKernelServiceProvider
 * @package DailyFive\Provider
 */
class HttpKernelServiceProvider implements \Pimple\ServiceProviderInterface
{
    /**
     * Registers services on the given container.
     *
     * This method should only be used to configure services and parameters.
     * It should not get services.
     *
     * @param Container $pimple A container instance
     */
    public function register(Container $pimple)
    {
        $pimple['middleware_dispatcher'] = function ($pimple) {
            return new \DailyFive\Middleware\Dispatcher(
                $pimple,
                array()
            );
        };

        $pimple['controller_resolver'] = function ($pimple) {
            return new \DailyFive\Kernel\ControllerResolver($pimple);
        };

        $pimple['http_kernel'] = function ($pimple) {
            return new \DailyFive\Kernel\HttpKernel(
                $pimple,
                $pimple['request_context'],
                $pimple['url_matcher'],
                $pimple['routes'],
                $pimple['controller_resolver'],
                $pimple['middleware_dispatcher']
            );
        };
    }
}
