<?php

namespace DailyFive\Provider;

use Monolog\Handler\StreamHandler;
use Pimple\Container;

/**
 * Class LoggerServiceProvider
 * @package DailyFive\Provider
 */
class LoggerServiceProvider implements \Pimple\ServiceProviderInterface
{
    /**
     * Registers services on the given container.
     *
     * This method should only be used to configure services and parameters.
     * It should not get services.
     *
     * @param Container $pimple A container instance
     */
    public function register(Container $pimple)
    {
        $pimple['logger'] = function ($pimple) {
            return new \Monolog\Logger('app');
        };

        $pimple['kernel.logger'] = function ($pimple) {
            $logger = new \Monolog\Logger('kernel');

            $file = '/kernel.log';

            $logger->pushHandler(new StreamHandler($pimple['path.log'] . $file, \Monolog\Logger::DEBUG));
            $logger->pushHandler(new StreamHandler($pimple['path.log'] . $file, \Monolog\Logger::INFO));
            $logger->pushHandler(new StreamHandler($pimple['path.log'] . $file, \Monolog\Logger::NOTICE));
            $logger->pushHandler(new StreamHandler($pimple['path.log'] . $file, \Monolog\Logger::WARNING));
            $logger->pushHandler(new StreamHandler($pimple['path.log'] . $file, \Monolog\Logger::ERROR));
            $logger->pushHandler(new StreamHandler($pimple['path.log'] . $file, \Monolog\Logger::CRITICAL));
            $logger->pushHandler(new StreamHandler($pimple['path.log'] . $file, \Monolog\Logger::ALERT));
            $logger->pushHandler(new StreamHandler($pimple['path.log'] . $file, \Monolog\Logger::EMERGENCY));

            return $logger;
        };
    }
}
