<?php

namespace DailyFive\Provider;

use DailyFive\Routing\Router;
use DailyFive\Routing\UrlGenerator;
use DailyFive\Routing\UrlMatcher;
use Pimple\Container;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\RouteCollection;

/**
 * Class RoutingServiceProvider
 * @package DailyFive\Provider
 */
class RoutingServiceProvider implements \Pimple\ServiceProviderInterface
{
    /**
     * Registers services on the given container.
     *
     * This method should only be used to configure services and parameters.
     * It should not get services.
     *
     * @param Container $pimple A container instance
     */
    public function register(Container $pimple)
    {
        $pimple['route_class'] = 'DailyFive\\Routing\\Route';

        $pimple['route_factory'] = $pimple->factory(function ($pimple) {
            return new $pimple['route_class']();
        });

        $pimple['routes_factory'] = $pimple->factory(function () {
            return new RouteCollection();
        });

        $pimple['routes'] = function ($pimple) {
            return $pimple['routes_factory'];
        };

        $pimple[Router::class] = function ($pimple) {
            return new Router(
                $pimple['routes']
            );
        };

        $pimple[RequestContext::class] = function ($pimple) {
            $context = new RequestContext();

            $context->setHttpPort(isset($pimple['request.http_port']) ? $pimple['request.http_port'] : 80);
            $context->setHttpPort(isset($pimple['request.https_port']) ? $pimple['request.https_port'] : 443);

            return $context;
        };

        $pimple[UrlMatcher::class] = function ($pimple) {
            return new UrlMatcher();
        };

        $pimple[UrlGenerator::class] = function ($pimple) {
            return new UrlGenerator(
                $pimple['routes'],
                $pimple[RequestContext::class]
            );
        };
    }

    /**
     * @return array
     */
    public function alias()
    {
        return [
            'url_generator' => UrlGenerator::class,
            'router' => Router::class,
            'request_context' => RequestContext::class,
            'url_matcher' => UrlMatcher::class,
        ];
    }
}
