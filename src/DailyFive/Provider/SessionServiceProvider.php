<?php

namespace DailyFive\Provider;

use DailyFive\Session\Session;
use Pimple\Container;
use Symfony\Component\HttpFoundation\Session\Storage\Handler\NativeFileSessionHandler;
use Symfony\Component\HttpFoundation\Session\Storage\NativeSessionStorage;

/**
 * Class SessionServiceProvider
 * @package DailyFive\Provider
 */
class SessionServiceProvider implements \Pimple\ServiceProviderInterface
{
    /**
     * Registers services on the given container.
     *
     * This method should only be used to configure services and parameters.
     * It should not get services.
     *
     * @param Container $pimple A container instance
     */
    public function register(Container $pimple)
    {
        $pimple[Session::class] = function ($pimple) {
            return new Session(
                $pimple[NativeSessionStorage::class],
                $pimple['session.attribute_bag'],
                $pimple['session.flash_bag']
            );
        };

        $pimple[NativeFileSessionHandler::class] = function ($pimple) {
            return new NativeFileSessionHandler(
                $pimple['session.storage.save_path']
            );
        };

        $pimple[NativeSessionStorage::class] = function ($pimple) {
            return new NativeSessionStorage(
                $pimple['session.storage.options'],
                $pimple[NativeFileSessionHandler::class]
            );
        };

        $pimple['session.storage.options'] = array();
        $pimple['session.default_locale'] = 'en';
        $pimple['session.storage.save_path'] = null;
        $pimple['session.attribute_bag'] = null;
        $pimple['session.flash_bag'] = null;
    }

    /**
     * @return array
     */
    public function alias()
    {
        return [
            'session' => Session::class,
            'session.storage.handler' => NativeFileSessionHandler::class,
            'session.storage.native' => NativeSessionStorage::class,
        ];
    }
}
