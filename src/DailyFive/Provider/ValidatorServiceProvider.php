<?php

namespace DailyFive\Provider;

use Pimple\Container;
use Pimple\ServiceProviderInterface;
use Symfony\Component\Validator\ConstraintValidatorFactory;
use Symfony\Component\Validator\Mapping\Factory\LazyLoadingMetadataFactory;
use Symfony\Component\Validator\Mapping\Loader\StaticMethodLoader;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Validator\RecursiveValidator;
use Symfony\Component\Validator\ValidatorBuilder;

/**
 * Class ValidatorServiceProvider
 * @package DailyFive\Provider
 */
class ValidatorServiceProvider implements ServiceProviderInterface
{

    /**
     * Registers services on the given container.
     *
     * This method should only be used to configure services and parameters.
     * It should not get services.
     *
     * @param Container $pimple A container instance
     */
    public function register(Container $pimple)
    {
        $pimple[RecursiveValidator::class] = function ($pimple) {
            return $pimple[ValidatorBuilder::class]->getValidator();
        };
        $pimple[ValidatorBuilder::class] = function ($pimple) {
            $builder = Validation::createValidatorBuilder();
            $builder->setConstraintValidatorFactory($pimple['validator.validator_factory']);
            $builder->setTranslationDomain('validators');
            $builder->addObjectInitializers($pimple['validator.object_initializers']);
            $builder->setMetadataFactory($pimple['validator.mapping.class_metadata_factory']);
            if (isset($pimple['translator'])) {
                $builder->setTranslator($pimple['translator']);
            }
            return $builder;
        };
        $pimple['validator.mapping.class_metadata_factory'] = function ($pimple) {
            return new LazyLoadingMetadataFactory(new StaticMethodLoader());
        };
        $pimple['validator.validator_factory'] = function () {
            return new ConstraintValidatorFactory();
        };
        $pimple['validator.object_initializers'] = [];
        $pimple['validator.validator_service_ids'] = [];
    }

    /**
     * @return array
     */
    public function alias()
    {
        return [
            'validator' => RecursiveValidator::class,
        ];
    }
}
