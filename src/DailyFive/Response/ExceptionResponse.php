<?php

namespace DailyFive\Response;

use Symfony\Component\HttpFoundation\Response;

/**
 * Class ExceptionResponse
 * @package DailyFive\Exceptions
 */
class ExceptionResponse extends Response
{
    /**
     * @var \Exception
     */
    protected $exception;

    /**
     * ExceptionResponse constructor.
     *
     * @param \Exception $ex
     * @param int        $status
     * @param array      $headers
     */
    public function __construct(\Exception $ex, $status = 200, array $headers = array())
    {
        parent::__construct('', $status, $headers);

        $this->setException($ex);
        $this->createContent($ex);
    }

    /**
     * @return \Exception
     */
    public function getException()
    {
        return $this->exception;
    }

    /**
     * @param \Exception $exception
     *
     * @return void
     */
    public function setException(\Exception $exception)
    {
        $this->exception = $exception;
    }

    /**
     * @param \Exception $ex
     *
     * @return void
     */
    protected function createContent(\Exception $ex)
    {
        $template = '<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>' . $ex->getMessage() . '</title>
    <style>
        /* CSS Reset */
        html { box-sizing: border-box; }
        *, *::before, *::after { box-sizing: inherit; }
        * {margin: 0; padding:0;}
        
        body { font-family: "Open Sans", sans-serif; line-height: 1.5; color: black; font-size: 14px }
        
        .container {margin: 0 auto; max-width: 1200px; padding: 40px;}
        
        table { font-size:inherit; border-collapse: collapse; width: 100%; }
        table td, table th { padding: .25em; text-align: left; }
        table th { text-transform: uppercase; }
        table td { vertical-align: top; }
        table tbody tr:last-child { background-color: #ffebee; }
        table tbody tr:last-child td{ color: #f44336 }
        
        .message { display: block; margin: 20px 0; color: #f44336; }
        

    </style>
</head>
<body>
    <div class="container">
        <h4>' . get_class($ex) . '</h4>
        <small>' . $ex->getFile() . '</small>
        
        <p class="message">' . $ex->getMessage() . '</p>
        <hr>
        ' . $this->createTrace(array_reverse($ex->getTrace())) . '
    </div>
</body>
</html>';

        $this->setContent($template);
    }

    /**
     * @todo integrate all backtrace values; missing values: object, args
     * @link https://secure.php.net/manual/de/function.debug-backtrace.php
     *
     * @param array $trace
     *
     * @return string
     */
    protected function createTrace(array $trace)
    {
        $content = '<table>
    <thead>
        <tr>
            <th>#</th>
            <th>file</th>
            <th>class</th>
            <th>type</th>
            <th>function</th>
            <th>line</th>
        </tr>
    </thead>
    <tbody>';

        foreach ($trace as $key => $val) {
            $file = isset($val['file']) ? $val['file'] : '';
            $class = isset($val['class']) ? $val['class'] : '';
            $type = isset($val['type']) ? $val['type'] : '';
            $function = isset($val['function']) ? $val['function'] : '';
            $line = isset($val['line']) ? $val['line'] : '';

            $content .= '
        <tr>
            <td>' . $key . '</td>
            <td>' . $file . '</td>
            <td>' . $class . '</td>
            <td>' . $type . '</td>
            <td>' . $function . '</td>
            <td>' . $line . '</td>
        </tr>';
        }
        $content .= '</tbody></table>';

        return $content;
    }
}
