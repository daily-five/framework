<?php

namespace DailyFive\Response;

use \Symfony\Component\HttpFoundation\RedirectResponse as SymfonyRedirectResponse;

/**
 * Class RedirectResponse
 * @package DailyFive\Response
 */
class RedirectResponse extends SymfonyRedirectResponse
{
    /**
     * RedirectResponse constructor.
     *
     * @param       $url
     * @param int   $status
     * @param array $headers
     */
    public function __construct($url, $status = 302, array $headers = array())
    {
        parent::__construct($url, $status, $headers);
    }
}
