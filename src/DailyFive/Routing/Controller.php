<?php

namespace DailyFive\Routing;

use \BadMethodCallException;

/**
 * Class Controller
 * @package DailyFive\Routing
 */
abstract class Controller
{
    /**
     * Execute an action on the controller.
     *
     * @param  string  $method
     * @param  array   $parameters
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function callAction($method, array $parameters)
    {
        return call_user_func_array([$this, $method], $parameters);
    }

    /**
     * Handle calls to missing methods on the controller.
     *
     * @param $name
     * @param $arguments
     *
     * @return void
     * @throws \BadMethodCallException
     */
    public function __call($name, $arguments)
    {
        throw new BadMethodCallException("Method '{$name}' does not exist.");
    }
}
