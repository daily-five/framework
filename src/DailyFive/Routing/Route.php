<?php

namespace DailyFive\Routing;

use Symfony\Component\Routing\Route as BaseRoute;

/**
 * Class Route
 * @package DailyFive\Routing
 */
class Route extends BaseRoute
{
    /**
     * Route constructor.
     *
     * @param string $path
     * @param array  $defaults
     * @param array  $requirements
     * @param array  $options
     * @param string $host
     * @param array  $schemes
     * @param array  $methods
     * @param string $condition
     */
    public function __construct(
        $path = '',
        array $defaults = array(),
        array $requirements = array(),
        array $options = array(),
        $host = '',
        $schemes = array(),
        $methods = array(),
        $condition = ''
    ) {
        parent::__construct($path, $defaults, $requirements, $options, $host, $schemes, $methods, $condition);
    }

    /**
     * @param string|array $middleware
     *
     * @return void
     */
    public function addMiddleware($middleware)
    {
        $default = (array) $this->getDefault('options');
        if (isset($default['middleware'])) {
            $default['middleware'] = array_unique(
                array_merge(
                    (array) $middleware,
                    (array) $default['middleware']
                ),
                SORT_STRING
            );
        } else {
            $default['middleware'] = array_unique((array) $middleware, SORT_STRING);
        }
        $this->setDefault('options', $default);
    }

    /**
     * @return array|null
     */
    public function getMiddleware()
    {
        $default = (array) $this->getDefault('options');
        return isset($default['middleware']) ? $default['middleware'] : null;
    }
}
