<?php

namespace DailyFive\Routing;

use Closure;
use LogicException;
use Symfony\Component\Routing\RouteCollection;

/**
 * Class Router
 * @package DailyFive\Routing
 */
class Router
{
    const CONTROLLER_METHOD_DELIMITER = '::';

    /**
     * @var \Symfony\Component\Routing\RouteCollection
     */
    protected $routes;

    /**
     * Router constructor.
     *
     * @param \Symfony\Component\Routing\RouteCollection $routes
     */
    public function __construct(
        RouteCollection $routes
    ) {
        $this->routes = $routes;
    }

    /**
     * Create a new Route instance
     *
     * @return \DailyFive\Routing\Route
     */
    protected function createRoute()
    {
        return new Route();
    }

    /**
     * Create a new RouteCollection
     *
     * @return \Symfony\Component\Routing\RouteCollection
     */
    protected function createRouteCollection()
    {
        return new RouteCollection();
    }

    /**
     * @return \Symfony\Component\Routing\RouteCollection
     */
    public function getRoutes()
    {
        return $this->routes;
    }

    /**
     * @param string       $path
     * @param string       $controller
     * @param array        $options
     * @param string|array $requestMethods
     *
     * @return void
     */
    public function addRoute($path, $controller, array $options, $requestMethods)
    {
        $route = $this->createRoute();

        $controllerData = explode(self::CONTROLLER_METHOD_DELIMITER, $controller);

        if (count($controllerData) !== 2) {
            throw new LogicException(
                "Wrong Controller-Method input: '{$controller}'. " .
                "Expression must be '\{ControllerName\}::\{MethodName\}'"
            );
        }

        $route->setPath($path);
        $route->setDefault('_controller', $controllerData[0]);
        $route->setDefault('_method', $controllerData[1]);
        $route->setDefault('options', $options);
        $route->setMethods($requestMethods);

        $this->routes->add($this->createRouteName($route), $route);
    }

    /**
     * Creates the Route name
     *
     * @param \Symfony\Component\Routing\Route $route
     *
     * @return string
     */
    public function createRouteName(\Symfony\Component\Routing\Route $route)
    {
        $requestMethods = $route->getMethods();
        if (is_array($requestMethods)) {
            $requestMethods = implode('-', $requestMethods);
        }
        return str_slugify(strtolower($requestMethods) . $route->getPath());
    }

    /**
     * Adds a route for matching GET requests
     *
     * @param string $path
     * @param string $controller
     * @param array  $options
     *
     * @return void
     */
    public function get($path, $controller, array $options = array())
    {
        $this->addRoute($path, $controller, $options, 'GET');
    }

    /**
     * Adds a route for matching POST requests
     *
     * @param string $path
     * @param string $controller
     * @param array  $options
     *
     * @return void
     */
    public function post($path, $controller, array $options = array())
    {
        $this->addRoute($path, $controller, $options, 'POST');
    }

    /**
     * Adds a route for matching PUT requests
     *
     * @param string $path
     * @param string $controller
     * @param array  $options
     *
     * @return void
     */
    public function put($path, $controller, array $options = array())
    {
        $this->addRoute($path, $controller, $options, 'PUT');
    }

    /**
     * Adds a route for matching PATCH requests
     *
     * @param string $path
     * @param string $controller
     * @param array  $options
     *
     * @return void
     */
    public function patch($path, $controller, array $options = array())
    {
        $this->addRoute($path, $controller, $options, 'PATCH');
    }

    /**
     * Adds a route for matching DELETE requests
     *
     * @param string $path
     * @param string $controller
     * @param array  $options
     *
     * @return void
     */
    public function delete($path, $controller, array $options = array())
    {
        $this->addRoute($path, $controller, $options, 'DELETE');
    }

    /**
     * Adds a route for each request method
     *
     * @param array  $requestMethods
     * @param string $path
     * @param string $controller
     * @param array  $options
     *
     * @return void
     */
    public function match(array $requestMethods, $path, $controller, array $options = array())
    {
        $this->addRoute($path, $controller, $options, $requestMethods);
    }

    /**
     * Adds a RouteGroup to RouteCollection
     *
     * @param string   $prefix
     * @param array    $options
     * @param \Closure $callback
     *
     * @return void
     */
    public function group($prefix, array $options, Closure $callback)
    {
        // New RoutesCollection with prefix
        $routesCollection = $this->createRouteCollection();

        // Create a new Router instance
        $subRouter = new self($routesCollection);

        $callback($subRouter);

        foreach ($routesCollection->all() as $route) {
            $route->setPath('/' . $prefix . $route->getPath());

            if (method_exists($route, 'addMiddleware')
                && isset($options['middleware'])
                && $options['middleware']) {
                $route->addMiddleware($options['middleware']);
            }
            $this->routes->add($this->createRouteName($route), $route);
        }
    }
}
