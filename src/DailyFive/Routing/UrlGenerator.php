<?php

namespace DailyFive\Routing;

use Symfony\Component\Routing\Generator\UrlGenerator as SymfonyUrlGenerator;

/**
 * Class UrlGenerator
 * @package DailyFive\Routing
 */
class UrlGenerator extends SymfonyUrlGenerator
{
    /**
     * @param string $relativePath
     *
     * @return string
     */
    public function getUrl($relativePath = '/')
    {
        return $this->context->getBaseUrl() . '/' . trim($relativePath, '/');
    }
}
