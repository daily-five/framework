<?php

namespace DailyFive\Routing;

use Symfony\Component\Routing\Matcher\UrlMatcher as SymfonyUrlMatcher;
use Symfony\Component\Routing\RouteCollection;

/**
 * Class UrlMatcher
 * @package DailyFive\Routing
 */
class UrlMatcher extends SymfonyUrlMatcher
{
    /**
     * UrlMatcher constructor.
     *
     * @param \Symfony\Component\Routing\RouteCollection|null $routes
     * @param \Symfony\Component\Routing\RequestContext|null  $context
     */
    public function __construct(
        \Symfony\Component\Routing\RouteCollection $routes = null,
        \Symfony\Component\Routing\RequestContext $context = null
    ) {
        $this->routes = $routes;
        $this->context = $context;
    }

    /**
     * @param string                                     $pathinfo
     * @param \Symfony\Component\Routing\RouteCollection $routes
     *
     * @return array
     */
    public function matchRoutesCollection($pathinfo, RouteCollection $routes)
    {
        return $this->matchCollection($pathinfo, $routes);
    }
}
