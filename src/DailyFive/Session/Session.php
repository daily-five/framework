<?php

namespace DailyFive\Session;

use DailyFive\Request\Request;
use DailyFive\Support\Arrayable;
use Symfony\Component\HttpFoundation\Session\Session as SymfonySession;

/**
 * Class Session
 * @package DailyFive\Session
 */
class Session extends SymfonySession
{
    /**
     * Buffer flashed data
     *
     * @var array
     */
    protected $flashStore;

    /**
     * Buffer errors
     *
     * @var array
     */
    protected $errors;

    /**
     * Buffer old request data
     *
     * @var array
     */
    protected $oldRequest;
    
    /**
     * Session constructor.
     *
     * @param \Symfony\Component\HttpFoundation\Session\Storage\SessionStorageInterface|null $storage
     * @param \Symfony\Component\HttpFoundation\Session\Attribute\AttributeBagInterface|null $attributes
     * @param \Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface|null         $flashes
     */
    public function __construct(
        \Symfony\Component\HttpFoundation\Session\Storage\SessionStorageInterface $storage = null,
        \Symfony\Component\HttpFoundation\Session\Attribute\AttributeBagInterface $attributes = null,
        \Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface $flashes = null
    ) {
        parent::__construct($storage, $attributes, $flashes);
    }

    /**
     * @param null|string|array $key
     * @param null|string|array $value
     *
     * @return string|array
     */
    public function flash($key = null, $value = null)
    {
        if (! $value && ! $this->flashStore) {
            $this->flashStore = $this->getFlashBag()->get('_flash.data');
        }
        if (is_array($key)) {
            $this->flashStore = array_merge($this->flashStore, $key);
        } elseif ($key && $value) {
            $this->flashStore[$key] = $value;
        } elseif ($key && !$value) {
            return isset($this->flashStore[$key]) ? $this->flashStore[$key] : null;
        } elseif (!$key) {
            return $this->flashStore;
        }
        $this->getFlashBag()->set('_flash.data', $this->flashStore);
    }

    /**
     * Flash the current request
     *
     * @param \DailyFive\Request\Request $request
     *
     * @return $this
     */
    public function flashRequest(Request $request)
    {
        $this->getFlashBag()->set(
            '_flash.request',
            $this->removePasswords($request->getInputSource()->all())
        );
        return $this;
    }

    /**
     * Return an old request data
     *
     * @param string      $key
     * @param null|string $default
     *
     * @return mixed
     */
    public function old($key, $default = null)
    {
        if (! $this->oldRequest) {
            $this->oldRequest = $this->getFlashBag()->get('_flash.request');
        }
        return isset($this->oldRequest[$key]) ? $this->oldRequest[$key] : $default;
    }

    /**
     * Remove passwords from request
     *
     * @param array $data
     *
     * @return array
     */
    protected function removePasswords(array $data)
    {
        unset($data['password']);
        unset($data['passwort']);
        unset($data['pass']);
        return $data;
    }

    /**
     * Flash errors
     *
     * @param array|Arrayable $errors
     *
     * @return void
     */
    public function flashErrors($errors)
    {
        if ($errors instanceof Arrayable) {
            $this->getFlashBag()->set('_flash.errors', $errors->toArray());
        } elseif (is_array($errors)) {
            $this->getFlashBag()->set('_flash.errors', $errors);
        }
    }

    /**
     * Return flashed errors
     *
     * @return array
     */
    public function getErrors()
    {
        if (! $this->errors) {
            $this->errors = $this->getFlashBag()->get('_flash.errors');
        }
        return $this->errors;
    }
}
