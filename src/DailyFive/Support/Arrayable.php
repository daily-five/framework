<?php

namespace DailyFive\Support;

/**
 * Interface Arrayable
 * @package DailyFive\Support
 */
interface Arrayable
{
    /**
     * Get the instance as an array
     *
     * @return array
     */
    public function toArray();
}
