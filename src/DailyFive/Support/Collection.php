<?php

namespace DailyFive\Support;

use Countable;

/**
 * Class Collection
 * @package DailyFive\Support
 */
class Collection implements Countable, Arrayable, Jsonable
{
    /**
     * @var array
     */
    protected $items = array();

    /**
     * Collection constructor.
     *
     * @param array $items
     */
    public function __construct(array $items = array())
    {
        $this->items = $items;
    }

    /**
     * @return array
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * @param array $items
     */
    public function setItems(array $items)
    {
        $this->items = $items;
    }

    /**
     * Add an item to Collection
     *
     * @param mixed $item
     */
    public function addItem($item)
    {
        $this->items[] = $item;
    }

    /**
     * Wrapper for addItem()
     *
     * @param mixed $item
     */
    public function add($item)
    {
        $this->addItem($item);
    }

    /**
     * @param callable $callback
     *
     * @return \DailyFive\Support\Collection
     */
    public function map(callable $callback)
    {
        $keys = array_keys($this->items);

        $items = array_map($callback, $this->items, $keys);

        return new self(array_combine($keys, $items));
    }

    /**
     * @param callable|null $callback
     *
     * @return \DailyFive\Support\Collection
     */
    public function filter(callable $callback = null)
    {
        if ($callback) {
            if (version_compare(phpversion(), '5.6.0', '>=')) {
                // 1 := ARRAY_FILTER_USE_BOTH
                return new self(array_filter($this->items, $callback, 1));
            }
            $data = array();
            foreach ($this->items as $key => $value) {
                $cb = call_user_func_array($callback, array($value, $key));
                if ($cb) {
                    $data[$key] = $cb;
                }
            }
            return new self($data);
        }
        return new self(array_filter($this->items));
    }

    /**
     * Count elements of an object
     * @link  http://php.net/manual/en/countable.count.php
     * @return int The custom count as an integer.
     * </p>
     * <p>
     * The return value is cast to an integer.
     * @since 5.1.0
     */
    public function count()
    {
        return count($this->items);
    }

    /**
     * Returns the representation of the instance as an array
     * All all items will be also converted to an array if it is Arrayable
     *
     * @return array
     */
    public function toArray()
    {
        $items = array();
        foreach ($this->items as $key => $item) {
            if ($item instanceof Arrayable) {
                $items[$key] = $item->toArray();
            } else {
                $items[$key] = $item;
            }
        }
        return $items;
    }

    /**
     * Returns the representation of the instance as JSON
     *
     * @param int $options
     *
     * @return string
     */
    public function toJson($options = 0)
    {
        return json_encode($this->toArray(), $options);
    }

    /**
     * Returns the first item
     *
     * @return mixed|null
     */
    public function first()
    {
        return isset($this->items[0]) ? $this->items[0] : null;
    }

    /**
     * Returns the last item
     *
     * @return mixed|null
     */
    public function last()
    {
        $count = $this->count();
        return $count ? $this->items[$count - 1] : null;
    }
}
