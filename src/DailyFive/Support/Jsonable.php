<?php

namespace DailyFive\Support;

/**
 * Interface Jsonable
 * @package DailyFive\Support
 */
interface Jsonable
{
    /**
     * Convert the object to JSON
     *
     * @param int $options
     *
     * @return string
     */
    public function toJson($options = 0);
}
