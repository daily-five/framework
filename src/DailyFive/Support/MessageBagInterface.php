<?php

namespace DailyFive\Support;

interface MessageBagInterface
{
    /**
     * Get the keys present in the message bag
     *
     * @return array
     */
    public function keys();

    /**
     * Add a message to the bag
     *
     * @param string $key
     * @param string $message
     *
     * @return $this
     */
    public function add($key, $message);

    /**
     * Merge a new array of messages into the bag
     *
     * @param \DailyFive\Support\MessageBag|array $messages
     *
     * @return $this
     */
    public function merge($messages);

    /**
     * Determine if messages exist for a given key
     *
     * @param string|array $key
     *
     * @return bool
     */
    public function has($key);

    /**
     * Get the first message from the bag for a given key
     *
     * @param string $key
     *
     * @return string
     */
    public function first($key = null);

    /**
     * Get all of the messages from the bag for a given key
     *
     * @param string $key
     *
     * @return array
     */
    public function get($key);

    /**
     * Get all of the messages for every key in the bag
     *
     * @return array
     */
    public function all();

    /**
     * Determine if the message bag has any messages
     *
     * @return bool
     */
    public function isEmpty();

    /**
     * Get the number of messages in the container
     *
     * @return int
     */
    public function count();

    /**
     * Get the instance as an array
     *
     * @return array
     */
    public function toArray();
}
