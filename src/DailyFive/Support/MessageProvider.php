<?php

namespace DailyFive\Support;

/**
 * Interface MessageProvider
 * @package DailyFive\Support
 */
interface MessageProvider
{
    /**
     * @return \DailyFive\Support\MessageBag
     */
    public function getMessageBag();
}
