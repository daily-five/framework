<?php

namespace DailyFive\Tests;

use DailyFive\Application;
use DailyFive\Middleware\Dispatcher;
use DailyFive\Middleware\TestMiddleware;
use DailyFive\Tests\Controller\NamespaceController;
use PHPUnit\Framework\TestCase;


class ApplicationTest extends TestCase
{
    public function testConstructorInjection()
    {
        $rootPath = root_path();
        $app = new Application(array(
            'path' => $rootPath,
        ));

        $this->assertEquals($rootPath, $app['path']);
    }

    public function testMiddlewareRegistration()
    {
        $app = new Application(array('path'=>root_path()));

        $app->addMiddleware('test', function () {
            return new TestMiddleware();
        });

        $this->assertInstanceOf(\DailyFive\Middleware\TestMiddleware::class, $app[Dispatcher::MIDDLEWARE_NAMESPACE.'test']);
    }

    public function testApplicationControllerActionCall()
    {
        $app = new Application(array('path'=>root_path()));

        $app['TestController'] = function () {
            return new \DailyFive\Tests\Controller\TestController();
        };

        $app->get('/', 'TestController::index');

        ob_start();
        $app->run();

        $ob = ob_get_contents();
        ob_end_clean();

        $this->assertEquals('Test', $ob);
    }

    public function testApplicationNamespaceControllerActionCall()
    {
        $app = new Application(array(
            'path' => root_path(),
            'namespace.app.root' => 'DailyFive\\Tests',
        ));

        $app[NamespaceController::class] = function () {
            return new NamespaceController();
        };

        $app->get('/', 'NamespaceController::index');

        ob_start();
        $app->run();

        $ob = ob_get_contents();
        ob_end_clean();

        $this->assertEquals('Test', $ob);
    }
}