<?php

namespace DailyFive\Tests\Config;

use DailyFive\Config\Config;
use PHPUnit\Framework\TestCase;


/**
 * Class ConfigTest
 * @package DailyFive\Tests\Config
 */
class ConfigTest extends TestCase
{
    public function testWrongConfigPath()
    {
        $this->expectException(\LogicException::class);
        $config = new Config(__DIR__.'/noDir');
    }

    public function testLoadConfigFiles()
    {
        $config = new Config(__DIR__.'/config');

        $this->assertEquals(array('test' => 'config test', 'next_depth'=>array('test'=>'config test')), $config->get('test_config'));
    }

    public function testAddDataToConfig()
    {
        $config = new Config(__DIR__.'/config');

        $testData = array(
            'depth1' => array(
                'depth2' => 'test'
            ),
        );

        $config->set('conf', $testData);
        $config->set('conf.second_add', 23);
        $config->set('test1.test2.test3', 'value');

        $td1 = array('depth1'=>array('depth2'=>'test'),'second_add'=>23);

        $this->assertEquals($td1, $config->get('conf'));
        $this->assertEquals(array('depth2'=>'test'), $config->get('conf.depth1'));
        $this->assertEquals('test', $config->get('conf.depth1.depth2'));

        $this->assertEquals(array('test2'=>array('test3'=>'value')), $config->get('test1'));
        $this->assertEquals(array('test3'=>'value'), $config->get('test1.test2'));
        $this->assertEquals('value', $config->get('test1.test2.test3'));
    }

    public function testKeyCantBeANumber()
    {
        $this->expectException('LogicException');
        $c = new Config();
        $c->set(5, "lim");
    }

    public function testKeyCantBeAnEmptyString()
    {
        $this->expectException('LogicException');
        $c = new Config();
        $c->set("", "lim");
    }

    public function testKeyCantBeAnObject()
    {
        $this->expectException('LogicException');
        $c = new Config();
        $c->set(new \StdClass(), "lim");
    }

    public function testKeyCantBeANull()
    {
        $this->expectException('LogicException');
        $c = new Config();
        $c->set(null, "lim");
    }

    public function testSetZeroDepthValueToConfig()
    {
        $c = new Config();

        $c->set("test_string", "hello");
        $c->set("test_string", "world");
        $c->set("test_number", 5);

        $this->assertEquals(5, $c->get("test_number"));
        $this->assertEquals("world", $c->get("test_string"));
    }

    public function testDefaultValue()
    {
        $c = new Config();

        $this->assertNull($c->get("test_unknown_key"));
        $this->assertEquals("default", $c->get("test_unknown_key", "default"));
        $this->assertEquals("default", $c->get("test_unknown_key.def", "default"));
    }

    public function testSetFirstDepthValueToConfig()
    {
        $c = new Config();

        $c->set("test_string", array("hello" => "world"));

        $this->assertEquals(array("hello" => "world"), $c->get("test_string"));
        $this->assertEquals("world", $c->get("test_string.hello"));
    }

    public function testSetNthDepthValueToConfig()
    {
        $c = new Config();

        $c->set("test_string", array("hello" => array("world" => "test")));

        $this->assertEquals(array("hello" => array("world" => "test")), $c->get("test_string"));
        $this->assertEquals(array("world" => "test"), $c->get("test_string.hello"));
        $this->assertEquals("test", $c->get("test_string.hello.world"));
    }

    public function testSetFirstDepthValueToConfigByDotNotation()
    {
        $c = new Config();

        $c->set("test_string.hello", "world");

        $this->assertEquals(array("hello" => "world"), $c->get("test_string"));
        $this->assertEquals("world", $c->get("test_string.hello"));
    }

    public function testSetNthDepthValueToConfigByDotNotation()
    {
        $c = new Config();

        $c->set("test_string.hello.world", "test");

        $this->assertEquals(array("hello" => array("world" => "test")), $c->get("test_string"));
        $this->assertEquals(array("world" => "test"), $c->get("test_string.hello"));
        $this->assertEquals("test", $c->get("test_string.hello.world"));
    }
}
