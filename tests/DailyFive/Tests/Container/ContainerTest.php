<?php

namespace DailyFive\Tests\Container;

use DailyFive\Container\Container;
use DailyFive\Tests\Container\Fixtures;
use PHPUnit\Framework\TestCase;


class ContainerTest extends TestCase
{
    public function testAutoresolveSimple()
    {
        $c = new Container();

        $service = $c[Fixtures\Service::class];

        $this->assertInstanceOf(Fixtures\Service::class, $service);
    }

    public function testAutoresolveExtended()
    {
        $c = new Container();

        /** @var \DailyFive\Tests\Container\Fixtures\Autoresolve $a */
        $a = $c[Fixtures\Autoresolve::class];

        $this->assertInstanceOf(Fixtures\Autoresolve::class, $a);
        $this->assertInstanceOf(Fixtures\Service::class, $a->service);
        $this->assertIsArray($a->array);
        $this->assertNull($a->test);
        $this->assertIsString($a->str);
        $this->assertEquals('hello', $a->str);

        $this->assertInstanceOf(Fixtures\Service::class, $c[Fixtures\Service::class]);
    }

    /**
     * @expectedException \Pimple\Exception\FrozenServiceException
     * @expectedExceptionMessage Cannot override frozen service "DailyFive\Tests\Container\Fixtures\Autoresolve".
     */
    public function testOverridingAutoresolvedService()
    {
        $c = new Container();

        $a = $c[Fixtures\Autoresolve::class];

        $c[Fixtures\Autoresolve::class] = 'test';
    }

    public function testCreateAliasFromClass()
    {
        $c = new Container();

        $this->assertEquals('Autoresolve', $c->createAliasFromClass(Fixtures\Autoresolve::class));
    }

    public function testAliases()
    {
        $c = new Container();

        $a = $c[Fixtures\Autoresolve::class];

        $this->assertEquals(['Service', 'Autoresolve'], $c->aliases());
        $this->assertEquals(Fixtures\Service::class, $c->getAlias('Service'));
        $this->assertEquals(Fixtures\Autoresolve::class, $c->getAlias('Autoresolve'));
    }

    public function testWithAlias()
    {
        $c = new Container();

        $a = $c[Fixtures\Autoresolve::class];

        $this->assertSame($a, $c['Autoresolve']);
        $this->assertSame($c['Autoresolve']->service, $c['Service']);

        $c['fixtures.service'] = function () { return new Fixtures\Service(); };
        $c->setAlias('fixtures.service', Fixtures\Service::class);

        $this->assertInstanceOf(Fixtures\Service::class, $c['fixtures.service']);
    }

    public function testUnsetAlias()
    {
        $c = new Container();

        $a = $c[Fixtures\Autoresolve::class];

        unset($c['Autoresolve']);

        $this->assertEquals(['Service'], $c->aliases());

        unset($c[Fixtures\Service::class]);

        $this->assertEquals([], $c->aliases());
    }

    public function testServiceProviderWithAlias()
    {
        $c = new Container();

        $c->register(new Fixtures\ExtendedPimpleServiceProvider());

        $a = $c['service.alias'];
        $b = $c[Fixtures\Service::class];

        $this->assertSame($a, $b);
    }

    /**
     * @expectedException \ReflectionException
     */
    public function testClassResolverError()
    {
        $c = new Container();
        $c->resolveClass('Foo');
    }

    public function testClassResolver()
    {
        $c = new Container();

        $this->assertTrue($c->resolveClass(Fixtures\Service::class));
        $this->assertInstanceOf(Fixtures\Service::class, $c[Fixtures\Service::class]);
    }

    public function testSilentClassResolver()
    {
        $c = new Container();

        $this->assertFalse($c->resolveClass('Foo', true));
    }
}
