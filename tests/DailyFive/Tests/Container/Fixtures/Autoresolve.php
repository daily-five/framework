<?php


namespace DailyFive\Tests\Container\Fixtures;


class Autoresolve
{
    public $service;
    public $array;
    public $test;
    public $str;

    public function __construct(
        Service $service,
        array $array,
        $test,
        $str = 'hello'
    ) {
        $this->service = $service;
        $this->array = $array;
        $this->test = $test;
        $this->str = $str;
    }
}