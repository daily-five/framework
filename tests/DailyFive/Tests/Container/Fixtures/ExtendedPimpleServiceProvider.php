<?php

namespace DailyFive\Tests\Container\Fixtures;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

class ExtendedPimpleServiceProvider implements ServiceProviderInterface
{
    /**
     * Registers services on the given container.
     *
     * This method should only be used to configure services and parameters.
     * It should not get services.
     *
     * @param Container $pimple An Container instance
     */
    public function register(Container $pimple)
    {
        $pimple['service.alias'] = function () {
            return new Service();
        };
    }

    /**
     * @return array
     */
    public function alias()
    {
        return [
            Service::class => 'service.alias',
        ];
    }
}
