<?php

namespace DailyFive\Tests\Controller;

use DailyFive\Controller\BasicController;
use DailyFive\Tests\Support\ObjectArrayable;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class AutoresolveController
 * @package DailyFive\Tests\Controller
 */
class AutoresolveController extends BasicController
{
    protected  $arrayable;
    protected  $test;

    public function __construct(
        ObjectArrayable $arrayable,
        $test
    ) {
        $this->arrayable = $arrayable;
        $this->test = $test;
    }

    public function responseWrapper($content = '', $status = 200, $headers = array())
    {
        return $this->response($content, $status, $headers);
    }

    public function jsonWrapper($data = null, $status = 200, $headers = array(), $json = false)
    {
        return $this->json($data, $status, $headers, $json);
    }

    public function redirectWrapper($url, $status = 302, array $headers = array())
    {
        return $this->redirect($url, $status, $headers);
    }

    public function downloadWrapper($contentType, $filename, $content = '', $status = 200, $headers = array())
    {
        return $this->download($contentType, $filename, $content, $status, $headers);
    }

    public function index()
    {
        return $this->response('Test');
    }

    protected function nonPublicMethod()
    {
    }

    public function methodWithRequestArgument(Request $request)
    {
    }
}