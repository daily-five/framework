<?php

namespace DailyFive\Tests\Controller;

use PHPUnit\Framework\TestCase;


class BasicControllerTest extends TestCase
{
    public function testControllerMissingMethods()
    {
        $this->expectException(\BadMethodCallException::class);

        $controller = new TestController();

        $controller->test();
    }

    public function testControllerActionCall()
    {
        $controllerName = \DailyFive\Tests\Controller\TestController::class;
        $controller = new $controllerName();

        $response = $controller->callAction('index', array());

        $this->assertInstanceOf(\Symfony\Component\HttpFoundation\Response::class, $response);
    }

    public function testControllerResponseReturnsResponse()
    {
        $controller = new TestController();

        $this->assertInstanceOf(\Symfony\Component\HttpFoundation\Response::class, $controller->responseWrapper());
    }

    public function testControllerResponseReturnsJsonResponse()
    {
        $controller = new TestController();

        $this->assertInstanceOf(\Symfony\Component\HttpFoundation\JsonResponse::class, $controller->jsonWrapper());
    }

    public function testControllerResponseReturnsRedirectResponse()
    {
        $controller = new TestController();

        $this->assertInstanceOf(\Symfony\Component\HttpFoundation\RedirectResponse::class, $controller->redirectWrapper('/'));
    }

    public function testControllerResponseReturnsDownloadResponse()
    {
        $controller = new TestController();

        $res = $controller->downloadWrapper('text/plain', 'test.txt', 'test content');

        $this->assertInstanceOf(\Symfony\Component\HttpFoundation\Response::class, $res);

        $this->assertTrue($res->headers->has('Content-Type'));
        $this->assertEquals('text/plain', $res->headers->get('Content-Type'));

        $this->assertTrue($res->headers->has('Content-Disposition'));
        $this->assertEquals('attachment; filename="test.txt"', $res->headers->get('Content-Disposition'));

        $this->assertEquals('test content', $res->getContent());
    }
}