<?php

namespace DailyFive\Tests\Kernel;

use DailyFive\Application;
use DailyFive\Kernel\ControllerResolver;
use DailyFive\Tests\Controller\BadController;
use DailyFive\Tests\Controller\NamespaceController;
use DailyFive\Tests\Controller\TestController;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Request;


/**
 * Class ControllerResolverTest
 * @package DailyFive\Tests\Kernel
 */
class ControllerResolverTest extends TestCase
{
    protected $container;

    protected $request;

    protected function setUp()
    {
        $this->container = new Application(array(
            'path' => root_path(),
            'namespace.app.root' => 'DailyFive\\Tests',
        ));
        $this->container['TestController'] = function () {
            return new TestController();
        };
        $this->container['BadController'] = function () {
            return new BadController();
        };
        $this->container[NamespaceController::class] = function () {
            return new NamespaceController();
        };
        $this->request = Request::createFromGlobals();
    }

    public function testControllerResolverSuccessfullResolve()
    {
        $resolver = new ControllerResolver($this->container);

        $match = array(
            '_controller' => 'TestController',
            '_method' => 'index',
        );

        $resolver->resolve($this->request, $match);

        $this->assertEquals('TestController', $resolver->getControllerName());
        $this->assertEquals('index', $resolver->getMethodName());
        $this->assertEquals(array(), $resolver->getArguments());
        $this->assertEquals(array(), $resolver->getMiddleware());
    }

    public function testControllerResolverSuccessfullResolveNamespaceController()
    {
        $resolver = new ControllerResolver($this->container);

        $match = array(
            '_controller' => 'NamespaceController',
            '_method' => 'index',
        );

        $resolver->resolve($this->request, $match);

        $this->assertEquals('DailyFive\Tests\Controller\NamespaceController', $resolver->getControllerName());
        $this->assertEquals('index', $resolver->getMethodName());
        $this->assertEquals(array(), $resolver->getArguments());
        $this->assertEquals(array(), $resolver->getMiddleware());
    }

    public function testControllerResolverSuccessfullResolveAutoresolveController()
    {
        $resolver = new ControllerResolver($this->container);

        $match = array(
            '_controller' => 'AutoresolveController',
            '_method' => 'index',
        );

        $resolver->resolve($this->request, $match);

        $this->assertEquals('DailyFive\Tests\Controller\AutoresolveController', $resolver->getControllerName());
        $this->assertEquals('index', $resolver->getMethodName());
        $this->assertEquals(array(), $resolver->getArguments());
        $this->assertEquals(array(), $resolver->getMiddleware());
    }

    public function testControllerResolverSuccessfullResolveWithRequestArgument()
    {
        $resolver = new ControllerResolver($this->container);

        $match = array(
            '_controller' => 'TestController',
            '_method' => 'methodWithRequestArgument',
        );

        $resolver->resolve($this->request, $match);

        $this->assertEquals('TestController', $resolver->getControllerName());
        $this->assertEquals('methodWithRequestArgument', $resolver->getMethodName());
        $this->assertEquals(array($this->request), $resolver->getArguments());
        $this->assertEquals(array(), $resolver->getMiddleware());
    }

    public function testControllerResolverCallNonPublicMethod()
    {
        $this->expectException(\LogicException::class);
        $resolver = new ControllerResolver($this->container);

        $match = array(
            '_controller' => 'TestController',
            '_method' => 'nonPublicMethod',
        );

        $resolver->resolve($this->request, $match);
    }

    public function testControllerResolverCallMissingMethod()
    {
        $this->expectException(\BadMethodCallException::class);
        $resolver = new ControllerResolver($this->container);

        $match = array(
            '_controller' => 'TestController',
            '_method' => 'missingMethod',
        );

        $resolver->resolve($this->request, $match);
    }

    public function testControllerResolverCallBadController()
    {
        $this->expectException(\LogicException::class);
        $resolver = new ControllerResolver($this->container);

        $match = array(
            '_controller' => 'BadController',
            '_method' => 'index',
        );

        $resolver->resolve($this->request, $match);
    }
}