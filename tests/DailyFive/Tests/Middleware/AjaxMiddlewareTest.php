<?php

namespace DailyFive\Tests\Middleware;

use DailyFive\Middleware\AjaxMiddleware;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use PHPUnit\Framework\TestCase;


class AjaxMiddlewareTest extends TestCase
{
    public function testRequestIsNoAjaxRequest()
    {
        $middleware = new AjaxMiddleware();

        $response = $middleware->handle(Request::createFromGlobals(), function () {});

        $this->assertInstanceOf(JsonResponse::class, $response);
        $this->assertEquals(422, $response->getStatusCode());
    }

    public function testRequestIsAjaxRequest()
    {
        $middleware = new AjaxMiddleware();

        $request = Request::createFromGlobals();
        $request->headers->add(array(
            'X-Requested-With' => 'XMLHttpRequest'
        ));
        $response = $middleware->handle($request, function (Request $request) {
            return new JsonResponse(array('OK'));
        });

        $this->assertInstanceOf(JsonResponse::class, $response);
        $this->assertEquals(200, $response->getStatusCode());
    }
}
