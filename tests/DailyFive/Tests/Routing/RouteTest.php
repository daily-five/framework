<?php

namespace DailyFive\Tests\Routing;

use DailyFive\Routing\Route;
use PHPUnit\Framework\TestCase;


/**
 * Class RouteTest
 * @package DailyFive\Tests\Routing
 */
class RouteTest extends TestCase
{
    public function testRouteNoMiddlewareAvailable()
    {
        $route = new Route();

        $this->assertNull($route->getMiddleware());
    }

    public function testRouteAddMiddleware()
    {
        $route = new Route();
        $route->addMiddleware('test');

        $middleware = $route->getMiddleware();
        $this->assertCount(1, $middleware);
        $this->assertContains('test', $middleware);
    }

    public function testRouteAddMiddlewareMultipleTimes()
    {
        $route = new Route();
        $route->addMiddleware('test');
        $route->addMiddleware(array('test1', 'test2'));

        $middleware = $route->getMiddleware();
        $this->assertCount(3, $middleware);
        $this->assertContains('test', $middleware);
        $this->assertContains('test1', $middleware);
        $this->assertContains('test2', $middleware);
    }

    public function testRouteAddMiddlewareNoDuplicates()
    {
        $route = new Route();
        $route->addMiddleware('test');
        $route->addMiddleware(array('test', 'test2'));

        $middleware = $route->getMiddleware();
        $this->assertCount(2, $middleware);
        $this->assertContains('test', $middleware);
        $this->assertContains('test2', $middleware);
    }
}
