<?php

namespace DailyFive\Tests\Routing;

use PHPUnit\Framework\TestCase;


/**
 * Class RouterTest
 * @package DailyFive\Tests\Routing
 */
class RouterTest extends TestCase
{
    /**
     * @expectedException \LogicException
     */
    public function testRouterWrongControllerInput()
    {
        $routeCollection = new \Symfony\Component\Routing\RouteCollection();
        $router = new \DailyFive\Routing\Router($routeCollection);

        $router->get('/', 'TestController');
    }

    public function testRouterAddGetRoute()
    {
        $routeCollection = new \Symfony\Component\Routing\RouteCollection();
        $router = new \DailyFive\Routing\Router($routeCollection);

        $router->get('/test', 'TestController::index');

        $this->assertCount(1, $routeCollection->all());

        $route = $routeCollection->get('get_test');
        $methods = $route->getMethods();
        $this->assertCount(1, $methods);
        $this->assertContains('GET', $methods);
    }

    public function testRouterAddPostRoute()
    {
        $routeCollection = new \Symfony\Component\Routing\RouteCollection();
        $router = new \DailyFive\Routing\Router($routeCollection);

        $router->post('/', 'TestController::index');

        $this->assertCount(1, $routeCollection->all());

        $route = $routeCollection->get('post');
        $methods = $route->getMethods();
        $this->assertCount(1, $methods);
        $this->assertContains('POST', $methods);
    }

    public function testRouterAddPutRoute()
    {
        $routeCollection = new \Symfony\Component\Routing\RouteCollection();
        $router = new \DailyFive\Routing\Router($routeCollection);

        $router->put('/', 'TestController::index');

        $this->assertCount(1, $routeCollection->all());

        $route = $routeCollection->get('put');
        $methods = $route->getMethods();
        $this->assertCount(1, $methods);
        $this->assertContains('PUT', $methods);
    }

    public function testRouterAddPatchRoute()
    {
        $routeCollection = new \Symfony\Component\Routing\RouteCollection();
        $router = new \DailyFive\Routing\Router($routeCollection);

        $router->patch('/', 'TestController::index');

        $this->assertCount(1, $routeCollection->all());

        $route = $routeCollection->get('patch');
        $methods = $route->getMethods();
        $this->assertCount(1, $methods);
        $this->assertContains('PATCH', $methods);
    }

    public function testRouterAddDeleteRoute()
    {
        $routeCollection = new \Symfony\Component\Routing\RouteCollection();
        $router = new \DailyFive\Routing\Router($routeCollection);

        $router->delete('/', 'TestController::index');

        $this->assertCount(1, $routeCollection->all());

        $route = $routeCollection->get('delete');
        $methods = $route->getMethods();
        $this->assertCount(1, $methods);
        $this->assertContains('DELETE', $methods);
    }

    public function testRouterAddMatchRoute()
    {
        $routeCollection = new \Symfony\Component\Routing\RouteCollection();
        $router = new \DailyFive\Routing\Router($routeCollection);

        $router->match(array('GET', 'POST'), '/', 'TestController::index');

        $this->assertCount(1, $routeCollection->all());

        $route = $routeCollection->get('get_post');
        $methods = $route->getMethods();
        $this->assertCount(2, $methods);
        $this->assertContains('GET', $methods);
        $this->assertContains('POST', $methods);
    }

    public function testRouterGroup()
    {
        $routeCollection = new \Symfony\Component\Routing\RouteCollection();
        $router = new \DailyFive\Routing\Router($routeCollection);

        $router->group('/prefix', array('middleware' => 'auth'), function (\DailyFive\Routing\Router $router) {
            $router->match(array('PUT', 'PATCH'), '/test', 'TestController::index', array('middleware' => array('logger', 'auth')));
        });

        $route = $routeCollection->get('put_patch_prefix_test');
        $methods = $route->getMethods();
        $this->assertCount(2, $methods);
        $this->assertContains('PUT', $methods);
        $this->assertContains('PATCH', $methods);

        $middleware = $route->getMiddleware();
        $this->assertCount(2, $middleware);
        $this->assertEquals('auth', $middleware[0]);
        $this->assertEquals('logger', $middleware[1]);
    }
}