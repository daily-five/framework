<?php

namespace DailyFive\Tests\Support;

use \DailyFive\Support\Collection;
use PHPUnit\Framework\TestCase;

class CollectionTest extends TestCase
{
    public function testCollectionConstructorNoValue()
    {
        $collection = new Collection();

        $this->assertCount(0, $collection);
    }

    public function testCollectionConstructorAddValue()
    {
        $collection = new Collection(array(
            'test1', 'test2', 'test3',
        ));

        $this->assertCount(3, $collection);
    }

    public function testCollectionConstructorAddKeyValueArray()
    {
        $collection = new Collection(array(
            'test1' => 'value1',
            'test2' => 'value2',
            'test3' => 'value3',
        ));

        $this->assertCount(3, $collection);
    }

    public function testCollectionMapData()
    {
        $collection = new Collection(array(
            'test1' => 'value1',
            'test2' => 'value2',
            'test3' => 'value3',
        ));

        $mappedCollection = $collection->map(function ($item, $key) {
            if ($key === 'test2') {
                return $item;
            }
            return $item . '_test';
        });

        $this->assertCount(3, $mappedCollection);
        $this->assertInstanceOf('\DailyFive\Support\Collection', $mappedCollection);
        $this->assertEquals(array(
            'test1' => 'value1_test',
            'test2' => 'value2',
            'test3' => 'value3_test',
        ), $mappedCollection->toArray());
    }

    public function testCollectionFilterDataWithKey()
    {
        $collection = new Collection(array(
            'test1' => 'value1',
            'test2' => 'value2',
            'test3' => 'value3',
        ));

        $filteredCollection = $collection->filter(function ($item, $key) {
            if ($key !== 'test2') {
                return $item;
            }
        });

        $this->assertCount(2, $filteredCollection);
        $this->assertInstanceOf('\DailyFive\Support\Collection', $filteredCollection);
        $this->assertEquals(array(
            'test1' => 'value1',
            'test3' => 'value3',
        ), $filteredCollection->toArray());
    }

    public function testCollectionFilterDataWithoutKey()
    {
        $collection = new Collection(array(
            'test1' => 'value1',
            'test2' => 'value2',
            'test3' => 'value3',
        ));

        $filteredCollection = $collection->filter(function ($item) {
            if ($item !== 'value2') {
                return $item;
            }
        });

        $this->assertCount(2, $filteredCollection);
        $this->assertInstanceOf('\DailyFive\Support\Collection', $filteredCollection);
        $this->assertEquals(array(
            'test1' => 'value1',
            'test3' => 'value3',
        ), $filteredCollection->toArray());
    }

    public function testCollectionFilterDataWithoutCallback()
    {
        $collection = new Collection(array(
            'test1' => 'value1',
            'test2' => 'value2',
            'test3' => 'value3',
            null, '', 0
        ));

        $filteredCollection = $collection->filter();

        $this->assertCount(3, $filteredCollection);
        $this->assertInstanceOf('\DailyFive\Support\Collection', $filteredCollection);
        $this->assertEquals(array(
            'test1' => 'value1',
            'test2' => 'value2',
            'test3' => 'value3',
        ), $filteredCollection->toArray());
    }

    public function testGetFirstAndLastItem()
    {
        $collection = new Collection();

        $this->assertNull($collection->first());
        $this->assertNull($collection->last());

        $collection->setItems(array('test1'));

        $this->assertEquals('test1', $collection->first());
        $this->assertEquals('test1', $collection->last());

        $collection->setItems(array('test1', 'test2', 'test3',));

        $this->assertEquals('test1', $collection->first());
        $this->assertEquals('test3', $collection->last());
    }

    public function testAddItemToCollection()
    {
        $collection = new Collection();

        $this->assertNull($collection->first());
        $this->assertNull($collection->last());

        $collection->setItems(array('test1'));

        $this->assertEquals('test1', $collection->first());
        $this->assertEquals('test1', $collection->last());

        $collection->add('test2');
        $collection->addItem('test3');

        $this->assertEquals('test1', $collection->first());
        $this->assertEquals('test3', $collection->last());
    }
    
    public function testCollectionToArrayItemsArrayable()
    {
        $collection = new Collection();
        
        $collection->add(new ObjectArrayable('test1'));
        $collection->add(new ObjectArrayable('test2'));
        
        $expected = array(
            array('data'=>'test1'),
            array('data'=>'test2'),
        );
        $this->assertEquals($expected, $collection->toArray());
    }
    
    public function testCollectionToArrayItemsNotArrayable()
    {
        $collection = new Collection();
        
        $collection->add(new ObjectNotArrayable('test1'));
        $collection->add(new ObjectNotArrayable('test2'));
        
        $expected = array(
            new ObjectNotArrayable('test1'),
            new ObjectNotArrayable('test2'),
        );
        $this->assertEquals($expected, $collection->toArray());
    }
}
