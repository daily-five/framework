<?php

namespace UPC\Tests\UPC\Framework\Tests\Support;

use DailyFive\Support\MessageBag;
use DailyFive\Support\Collection;
use PHPUnit\Framework\TestCase as TestCase;

class MessageBagTest extends TestCase
{
    public function testUniqueness()
    {
        $container = new MessageBag;
        $container->add('foo', 'bar');
        $container->add('foo', 'bar');
        $messages = $container->getMessages();
        $this->assertEquals(array('bar'), $messages['foo']);
    }

    public function testMessagesAreAdded()
    {
        $container = new MessageBag;
        $container->setFormat(':message');
        $container->add('foo', 'bar');
        $container->add('foo', 'baz');
        $container->add('boom', 'bust');
        $messages = $container->getMessages();
        $this->assertEquals(array('bar', 'baz'), $messages['foo']);
        $this->assertEquals(array('bust'), $messages['boom']);
    }

    public function testMessagesMayBeMerged()
    {
        $container = new MessageBag(array('username' => array('foo')));
        $container->merge(array('username' => array('bar')));
        $this->assertEquals(array('username' => array('foo', 'bar')), $container->getMessages());
    }

    public function testMessageBagsCanBeMerged()
    {
        $container = new MessageBag(array('foo' => array('bar')));
        $otherContainer = new MessageBag(array('foo' => array('baz'), 'bar' => array('foo')));
        $container->merge($otherContainer);
        $this->assertEquals(array('foo' => array('bar', 'baz'), 'bar' => array('foo')), $container->getMessages());
    }

    public function testMessageBagsCanConvertToArrays()
    {
        $container = new MessageBag(array(
            new Collection(array('foo', 'bar')),
            new Collection(array('baz', 'qux')),
        ));
        $this->assertSame(array(array('foo', 'bar'), array('baz', 'qux')), $container->getMessages());
    }

    public function testGetReturnsArrayOfMessagesByKey()
    {
        $container = new MessageBag;
        $container->setFormat(':message');
        $container->add('foo', 'bar');
        $container->add('foo', 'baz');
        $this->assertEquals(array('bar', 'baz'), $container->get('foo'));
    }

    public function testGetReturnsArrayOfMessagesByImplicitKey()
    {
        $container = new MessageBag;
        $container->setFormat(':message');
        $container->add('foo.1', 'bar');
        $container->add('foo.2', 'baz');
        $this->assertEquals(array('foo.1' => array('bar'), 'foo.2' => array('baz')), $container->get('foo.*'));
    }

    public function testFirstReturnsSingleMessage()
    {
        $container = new MessageBag;
        $container->setFormat(':message');
        $container->add('foo', 'bar');
        $container->add('foo', 'baz');
        $messages = $container->getMessages();
        $this->assertEquals('bar', $container->first('foo'));
    }

    public function testFirstReturnsEmptyStringIfNoMessagesFound()
    {
        $container = new MessageBag;
        $container->setFormat(':message');
        $messages = $container->getMessages();
        $this->assertEquals('', $container->first('foo'));
    }

    public function testFirstReturnsSingleMessageFromDotKeys()
    {
        $container = new MessageBag;
        $container->setFormat(':message');
        $container->add('name.first', 'jon');
        $container->add('name.last', 'snow');
        $messages = $container->getMessages();
        $this->assertEquals('jon', $container->first('name.*'));
    }

    public function testHasIndicatesExistence()
    {
        $container = new MessageBag;
        $container->setFormat(':message');
        $container->add('foo', 'bar');
        $this->assertTrue($container->has('foo'));
        $this->assertFalse($container->has('bar'));
    }

    public function testHasAnyIndicatesExistence()
    {
        $container = new MessageBag;
        $container->setFormat(':message');
        $container->add('foo', 'bar');
        $container->add('bar', 'foo');
        $container->add('boom', 'baz');
        $this->assertTrue($container->hasAny(array('foo', 'bar')));
        $this->assertTrue($container->hasAny('foo', 'bar'));
        $this->assertTrue($container->hasAny(array('boom', 'baz')));
        $this->assertTrue($container->hasAny('boom', 'baz'));
        $this->assertFalse($container->hasAny(array('baz')));
        $this->assertFalse($container->hasAny('baz'));
        $this->assertFalse($container->hasAny('baz', 'biz'));
    }

    public function testHasIndicatesExistenceOfAllKeys()
    {
        $container = new MessageBag;
        $container->setFormat(':message');
        $container->add('foo', 'bar');
        $container->add('bar', 'foo');
        $container->add('boom', 'baz');
        $this->assertTrue($container->has(array('foo', 'bar', 'boom')));
        $this->assertFalse($container->has(array('foo', 'bar', 'boom', 'baz')));
        $this->assertFalse($container->has(array('foo', 'baz')));
    }

    public function testHasIndicatesNoneExistence()
    {
        $container = new MessageBag;
        $container->setFormat(':message');

        $this->assertFalse($container->has('foo'));
    }

    public function testAllReturnsAllMessages()
    {
        $container = new MessageBag;
        $container->setFormat(':message');
        $container->add('foo', 'bar');
        $container->add('boom', 'baz');
        $this->assertEquals(array('bar', 'baz'), $container->all());
    }

    public function testFormatIsRespected()
    {
        $container = new MessageBag;
        $container->setFormat('<p>:message</p>');
        $container->add('foo', 'bar');
        $container->add('boom', 'baz');
        $this->assertEquals('<p>bar</p>', $container->first('foo'));
        $this->assertEquals(array('<p>bar</p>'), $container->get('foo'));
        $this->assertEquals(array('<p>bar</p>', '<p>baz</p>'), $container->all());
        $this->assertEquals('bar', $container->first('foo', ':message'));
        $this->assertEquals(array('bar'), $container->get('foo', ':message'));
        $this->assertEquals(array('bar', 'baz'), $container->all(':message'));

        $container->setFormat(':key :message');
        $this->assertEquals('foo bar', $container->first('foo'));
    }

    public function testMessageBagReturnsCorrectArray()
    {
        $container = new MessageBag;
        $container->setFormat(':message');
        $container->add('foo', 'bar');
        $container->add('boom', 'baz');

        $this->assertEquals(array('foo' => array('bar'), 'boom' => array('baz')), $container->toArray());
    }

    public function testMessageBagReturnsExpectedJson()
    {
        $container = new MessageBag;
        $container->setFormat(':message');
        $container->add('foo', 'bar');
        $container->add('boom', 'baz');

        $this->assertEquals('{"foo":["bar"],"boom":["baz"]}', $container->toJson());
    }

    public function testCountReturnsCorrectValue()
    {
        $container = new MessageBag;
        $this->assertCount(0, $container);

        $container->add('foo', 'bar');
        $container->add('foo', 'baz');
        $container->add('boom', 'baz');

        $this->assertCount(3, $container);
    }

    public function testCountable()
    {
        $container = new MessageBag;

        $container->add('foo', 'bar');
        $container->add('boom', 'baz');

        $this->assertCount(2, $container);
    }

    public function testConstructor()
    {
        $messageBag = new MessageBag(array('country' => 'Azerbaijan', 'capital' => 'Baku'));
        $this->assertEquals(array('country' => array('Azerbaijan'), 'capital' => array('Baku')), $messageBag->getMessages());
    }

    public function testFirstFindsMessageForWildcardKey()
    {
        $container = new MessageBag;
        $container->setFormat(':message');
        $container->add('foo.bar', 'baz');
        $messages = $container->getMessages();
        $this->assertEquals('baz', $container->first('foo.*'));
    }
}
