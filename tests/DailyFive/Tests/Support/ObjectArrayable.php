<?php

namespace DailyFive\Tests\Support;

use DailyFive\Support\Arrayable;

class ObjectArrayable implements Arrayable
{
    protected $data;
    public function __construct($data)
    {
        $this->data = $data;
    }
    
    public function toArray()
    {
        return array(
            'data' => $this->data,
        );
    }
}