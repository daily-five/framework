<?php

namespace DailyFive\Tests\Support;

class ObjectNotArrayable
{
    protected $data;
    public function __construct($data)
    {
        $this->data = $data;
    }
}